#! /usr/bin/env python

import os
import shutil
import subprocess
import uuid
import glob, sys
import argparse
import errno
import logging
import numpy as np
from netCDF4 import Dataset

if __name__ == "__main__":

    # vertices from a reference file
    vertf='/lustre3/projects/CMIP6/sager/cmorised-results/EC-EARTH-AerChem-AerChemMIP-hist-piNTCF/piNF/CMIP6/AerChemMIP/EC-Earth-Consortium/EC-Earth3-AerChem/hist-piNTCF/r1i1p1f1/Omon/tob/gn/v20200624/tob_Omon_EC-Earth3-AerChem_hist-piNTCF_r1i1p1f1_gn_201301-201312.nc'
    #vertf='/nfs/home/users/sager/cmorize/cmor-fixer/nemo-vertices/nemo-vertices-ORCA1-t-grid.nc'
    dvert = Dataset(vertf, 'r')
    lonvertices = dvert.variables['vertices_longitude'][:]
    latvertices = dvert.variables['vertices_latitude'][:]
    
    # Find all files to fix
    rootdata="/lustre3/projects/CMIP6/sager/cmorised-results/9bad-original/*.nc"
    files = glob.glob(rootdata)

    for f in files:

        src_bak = '{}.bak'.format(f)
        shutil.copy2(src=f, dst=src_bak)
        
        dvar = Dataset(f, 'r+')
        dvar.variables['vertices_longitude'][:] = lonvertices[:]
        dvar.variables['vertices_latitude'][:] = latvertices[:]

        dvar.close()

    dvert.close()


