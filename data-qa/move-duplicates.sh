#! /usr/bin/env bash

set -eu

usage() {
    cat << EOT >&2
Usage:
   $(basename $0) [-h] [-x] [-36dm] EXPERIMENT YEAR

   List or/and Move duplicates or incomplete 3hr files to a dedicated trash (dir: temp-EXP-LEGNB)

Options
   -x: move duplicates, default is to only list them - BE SURE TO EDIT THE GREP COMMAND TO SET VERSION TO KEEP
   -3: check for incomplete 3hr     files (different name) instead of true duplicates (same name)
   -6: check for incomplete 6hr     files (different name) instead of true duplicates (same name)
   -d: check for incomplete daily   files (different name) instead of true duplicates (same name)
   -m: check for incomplete monthly files (different name) instead of true duplicates (same name)
EOT
}

# -- options
list=1
move=0
incomp_3hr=0
incomp_6hr=0
daily=0
monthly=0

while getopts "hx36dm" opt; do
    case "$opt" in
        h) usage
           exit 0 ;;
        x) move=1 ;;
        3) incomp_3hr=1;;
        6) incomp_6hr=1;;
        d) daily=1 ;;
        m) monthly=1 ;;
    esac
done
shift $((OPTIND-1))

# -- Arg
if [ "$#" -ne 2 ]; then
    echo; echo " NEED TWO ARGUMENTS!"; echo
    usage
    exit 1
fi

EXP=$1
YEAR=$2
legnb=$(printf %03d $(( $2 - 1849 )))

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    hspr) XPRMNT=histSST-piAer ; MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    pic4) XPRMNT=piClim-aer ;     MIP=RFMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

# -- CMOR results
if [[ $PLATFORM == 'RHINO' ]]
then
    odir=/lustre3/projects/CMIP6/sager/cmorised-results
elif [[ $PLATFORM == 'CCA' ]]
then
    odir=/scratch/ms/nl/nm6/cmorised-results
fi

dest=${odir}/temp-${EXP}-$legnb

cd ${odir}/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3-AerChem/${XPRMNT}/r1i1p1f1

echo "*II* Checking data in: $PWD"

##### DUP
if (( incomp_3hr ))
then
    dup=$(find . -type f -name "*_3hr_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01010130-*" \
               ! -name "*_3hr_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01010130-${YEAR}12312230.nc")
    
    dup=${dup}\ $(find . -type f -name "*_3hr_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01010000-*" \
                     ! -name "*_3hr_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01010000-${YEAR}12312100.nc")
elif (( incomp_6hr ))
then
    dup=$(find . -type f -name "*_6hrPlevPt_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01010000-*" \
               ! -name "*_6hrPlevPt_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01010000-${YEAR}12311800.nc")
elif (( daily ))
then
    BFR=$((YEAR-1))
    dup=$(find . -type f -name "*_*day_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}0101-*" \
               ! -name "*_*day_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}0101-${YEAR}1231.nc")
    dup=${dup}\ $(find . -type f -name "*_*day_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${BFR}1231-${YEAR}1231.nc")
elif (( monthly ))
then
    BFR=$((YEAR-1))
    dup=$(find . -type f -name "*_*mon*_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01-*" \
               ! -name "*_*mon*_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YEAR}01-${YEAR}12.nc")
    dup=${dup}\ $(find . -type f -name "*_*mon*_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${BFR}12-${YEAR}12.nc")
else
    fl=$(find . -name "*_g[nr]_${YEAR}*" | tr ' ' "\012" | sort )
    dup=$(for k in $fl
          do
              basename $k
          done | uniq -d )
    (( move )) && list=0        # avoid too many calls to find
fi

(( ${#dup} )) && ndup=$(echo $dup | tr ' ' '\012' | wc -l) || ndup=0
echo Found $ndup duplicates or incomplete files.

#### LIST DUP
if (( list ))
then
    for f in $dup
    do
        (( incomp_3hr || incomp_6hr || daily || monthly )) && echo $f || \
                ls -l $(find . -name $f)
    done
    #exit 0
fi

#### MOVE DUP
if (( move ))
then
    answer=n
    for f in $dup
    do
        set +e
        (( incomp_3hr || incomp_6hr || daily || monthly )) && old=$f || \
                old=$(find . -name $f | grep -v v20210806) ############### DOUBLE-CHECK THAT GREP ################
        set -e
        #echo \"$old\"
        for g in $old
        do
            echo; echo; echo $old | tr ' ' '\012'
            echo
            [[ $answer != [Aa] ]] && read -r -p "Move $g ([y]es,[a]ll)? " -n1 -s answer
            #set -x
            if [[ $answer = [Yy] || $answer = [Aa] ]]
            then
                newdir=$(dirname $g)
                mkdir -p $dest/$newdir
                mv $g $dest/$newdir
            fi
            #set +x
        done
    done
fi
