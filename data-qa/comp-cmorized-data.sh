#! /usr/bin/env bash

#SBATCH --job-name=comp
#SBATCH --partition=all
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=28
#SBATCH --account=proj-cmip6

# Compare cmorized data in two different directories.
# Initially made to test if the glitch is linked to  non-reported errors.
#
#  The glitch is one line in the submit script, just at the end:
#  ('caught signal', <cdo.Cdo object at 0x7f9a32090a90>, 15, <frame object at 0x7f9a320f1de0>)
#  without other error reported in the cmorization logs. It is happening only with IFS obviously.

#set -e

odir=/lustre3/projects/CMIP6/sager/cmorised-results

cmordir1=${odir}/EC-EARTH-AerChem-CMIP-piControl/pict/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-AerChem/piControl/r1i1p1f1
cmordir2=${odir}/EC-EARTH-AerChem-CMIP-piControl/pict-TEST/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-AerChem/piControl/r1i1p1f1

# YYYY defines the leg being compared
fl1=$(find $cmordir1 -name "*1875*")
fl2=$(find $cmordir2 -name "*1875*")

# basename for comparing sets
base1=$(for k in $fl1
       do 
           basename $k
       done)

base2=$(for k in $fl2
       do 
           basename $k
       done)

echo; echo "Extra files in set #2:"
comm -13 <(echo $base1 | tr ' ' '\012' | sort) <(echo $base2 | tr ' ' '\012' | sort)
echo; echo "Extra files in set #1:"
comm -23 <(echo $base1 | tr ' ' '\012' | sort) <(echo $base2 | tr ' ' '\012' | sort)
echo

echo "Number of files to test: "$(echo $base2 | wc -w)

i=0
# Comparing content
for f in $base2
do
    (( i += 1 ))
    printf "file %03d\t%s\n " $i "$f"
    f1=$(echo $fl1 | tr ' ' '\012' | grep $f)
    f2=$(echo $fl2 | tr ' ' '\012' | grep $f)
    (
        ttb_compare.py -v3 $f1 $f2
    )
done
#wait
