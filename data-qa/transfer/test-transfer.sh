#!/usr/bin/env bash

EXP=a4co

trap "" HUP

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP

cd $datadir/CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3-AerChem/${XPRMNT}/r1i1p1f1


SRC=SImon

## checking that huponexit is off.
#shopt

exit

# create file list
#rsync -avm --stats --safe-links --ignore-existing --dry-run \
#    --human-readable $SRC $DEST > /tmp/transfer.log
#rsync -av --dry-run $SRC $DEST > ./log/transfer.log

find $SRC -type f > filelist

for n in 1 2 3
do
    
    DEST=/zweden-disk-B/knmi-rdwk-disk-02/tst${n}

    rm backups.*
    split --number=l/${n} filelist backups.

    ls backups.*

    t1=$(date +%s)
    if [[ ${n} -eq 1 ]]
    then
        rsync -av --stats --files-from=filelist . $DEST &> rlog1
    else
        ls backups.* | parallel --results rlog${n} --linebuffer --verbose -j${n} rsync -av --stats --files-from={} . $DEST
    fi
    t2=$(date +%s)
    tr=$(date -d "0 -$t1 sec + $t2 sec" +%T)
    echo $n $tr

done

## # -- Thomas way (single thread)
## nohup bash -c 'while ! `rsync -a /lustre3/projects/CMIP6/reerink/cmorised-results/cmor-cmip-scenario-ssp1-2.6 /zweden-disk-A/knmi-rdwk-disk-02/`;do sleep 60; done' > nohup-log-rsync-s126.txt &
## # which could be started over the ssh. Where zweden-disk-A is one of
## # the two mounted disks plugged in into one o f the two USB ports on
## # the bull gateway (account needed). The while is needed to overcome
## # time outs, but this worked in the end very well.
