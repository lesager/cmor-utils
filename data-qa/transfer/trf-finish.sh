#!/usr/bin/env bash

set -eu

(( $# != 1 )) && echo NEED EXP ARGUMENT && exit 1

EXP=$1
DEST=/zweden-disk-A/knmi-rdwk-disk-05/$EXP

[[ ! -d $DEST ]] && echo "NODIR: $DEST" && exit 1

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
file_list="file.list."$EXP
checksum_file="sha256sums-"$EXP

# check that everything is there
set -x
cd $datadir
rsync -avni CMIP6 $DEST
set +x
rsync -av $datadir/$checksum_file $DEST

cd $DEST
find CMIP6 -type f > $file_list

# compare file_list
ls -l $file_list
#ls -l $datadir/$file_list
ls -l $datadir/filelistA

#diff <(sort $file_list) <(sort "$datadir/$file_list")
diff <(sort $file_list) <(sort "$datadir/filelistA")

