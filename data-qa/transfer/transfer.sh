#!/usr/bin/env bash

# Submit as:
#   nohup ./transfer.sh > trnsfrt-s002.out 2> trnsfrt-s002.err < /dev/null &
#   Ctrl-d

EXP=sst6

trap "" HUP

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP

cd $datadir

SRC=CMIP6

# create file list
#find $SRC -type f -name "*hr*" > filelistA
find $SRC -type f > filelistA

DEST=/zweden-disk-A/knmi-rdwk-disk-05/$EXP
mkdir -p $DEST

split -l 3000 filelistA backupsA.

t1=$(date +%s)
ls backupsA.* | parallel --results rlog --linebuffer --verbose -j2 rsync -av --stats --files-from={} . $DEST
t2=$(date +%s)
tr=$(date -d "0 -$t1 sec + $t2 sec" +%T)
echo $tr
