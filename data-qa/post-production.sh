#! /usr/bin/env bash

# POST = nctime + version + sftof + NON-CMIP6 + CMOR-fixer + Checksum

#OBSOLETE: POST = nctime + sftlf + version + sftof + SP + NON-CMIP6 (incl. PMx) + CMOR-fixer + Checksum

WARNING="This is not a script - only a TODO list for post production"

echo $WARNING
exit 1


cd data-qa

# --- LSM

#./sftlf.py $EXP                 # OBSOLETE since ece2cmor3 v1.8 (august 2021)

# --- Check for empty files/dir and auxiliary files

./check-cmor-output-files.sh -e $EXP ystart yend

   # repeat command with -m if any
   # may need to do that a few time because of nested directories

# --- Check number of files per year; move duplicates and incomplete files if any

./check-cmor-output-files.sh -f $EXP ystart yend

  # check for years with wrong number of files, for example:
  # 050 2209: 303
  # 051 2210: 303
  # 052 2211: 303
  # 053 2212: 303
  # 054 2213: 445 --> pb
  # 055 2214: 303
  # 056 2215: 303
  # 057 2216: 303

  # If any, move duplicates in a backup/trash dir
  ./move-duplicates.sh $EXP 2213 # list dup for year 2213.
            # From listing, find out how to *EDIT* the grep command in the
            # move-duplicates.sh script, then run it again with -x:
  ./move-duplicates.sh -x $EXP 2213 # remove dup for year 2213

  # If there are still too many files, look for incomplete files, with -md63 options (exclusive options)

  # Check again for empty dir if you moved files around
  
# --- Fix version
# Command above should already have listed the timestamp versions used
# in the output. Set all output to one version
  
./check-cmor-output-files.sh -v vYYYYMMDD [-m] $EXP ystart yend # Use -m to effectively MOVE all files under one version number

  # and answer y to overvwrite duplicates (happens with constant fields)

  # At this point, you should have a clean output, like this:
  #
  #   [459] (master *%=) >>> ./check-cmor-output-files.sh -e pict 2160 2260
  #   *II* Check for empty files/dir
  #   *II* Check for auxiliary files
  #   *II* Check version
  #   v20210116

# --- sftof.py (only if cmorizing NEMO output). Be sure that the path to cmorized data is correct

./sftof.py $EXP

#OBSOLETE  # --- Fix non-surface PMx if done with code with the pmx-levels-bug -- ONLY with pre-3.3.3.2 releases
#OBSOLETE  
#OBSOLETE  sbatch -n1 ./fix-AERmon-PMx.py $EXP  # rhino
#OBSOLETE  # EDIT and submit wrap-it.sh on cca
#OBSOLETE  
#OBSOLETE     # in addition, this moves modified files to the NON-CMIP6 data space --> check for empty dir again


# --- Set aside the non-CMIP6 data, which cannot be published

./move-aside-non-cmip6-output.py $EXP

   # then --> check for empty dir again

#OBSOLETE  # --- Create Amon/ps -- ONLY with pre-v1.8 ece2cmor3 release
#OBSOLETE  sbatch ./create-Amon-ps.sh -v v20200622 $EXP ystart yend
#OBSOLETE  
#OBSOLETE      # You MUST add and use the --write flag when calling nctxck from nctime if Amon/ps is computed from 3hr data


### Following is done in the platform subdir. One of the following:
cd ../knmi-rhino/
cd ../ecmwf-cca/
cd ../ecmwf-hpc2020/

# --- Run nctime on acompute node with a wrapper around "./check-cmor-output-files.sh -t"  

echo " No available yet: relies on python 2.6+, need dedicated installation" # HPC2020 
sbatch ./monitor-qa.sh $EXP 1850  # rhino
qsub check-output.job # cca (requires edit to set EXP, year min, year max)

# --- Run cmor-fixer - it will do nothing if everything is fine. Check the log and the list of modified files.

echo " No available yet: need dedicated installation" # HPC2020 
# one of these two (check for --dry flag!)
# List of modified files (or to be modified if using the --dry flag) is in the topdir of the cmor-fixer code
qsub quick-cmorfix.sh # cca
qsub fix-cmor-cca.sh  # cca

# --- checksum.sh TWICE: CMIP6 and non-CMIP6 (requires edit)

./checksum.sh $EXP   # rhino
./checksum-cca.sh $EXP   # cca
./checksum.sh $EXP     # HPC2020

# --- move NON-CMIP6 in different topdir (can be done and packed in a tarball in checksum.sh)

#rhino
mv {file.list.NON-$EXP,NON-CMIP6,sha256sums-NON-$EXP} /lustre2/projects/model_testing/sager/aerchemip/$EXP

#cca (tar and put on ECFS)
store-non-cmip6.sh $EXP

#hpc2020
