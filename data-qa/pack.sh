#!/bin/bash
#SBATCH --job-name=pack
#SBATCH --account=proj-aerchemmip
#SBATCH --nodes=1
#SBATCH --exclusive

set -eu

if [ "$#" -ne 2 ]; then
    echo; echo " NEED TWO ARGUMENTS:  NO MORE, NO LESS!"; echo
    echo Usage: $(basename $0)  EXP  PACKNAME
    exit 1
fi

EXP=$1
NAME=$2

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        usage
        exit 1
esac

if [[ $PLATFORM == 'RHINO' ]]
then
    datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
elif [[ $PLATFORM == 'CCA' ]]
then
    datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
fi

cd $datadir

root=CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3-AerChem/$XPRMNT/r1i1p1f1

tgt=EC-Earth3-AerChem-$XPRMNT-$NAME.tar

echo
echo $datadir/$tgt
echo

# Using a file list
flist=/lustre3/projects/CMIP6/sager/cmorised-results/flist
[[ -f $flist ]] && rm -f $flist

########## For Manu - monthly variables from 1970-2015 from hist
# for v in mmrss dms cl cli clivi clt clw clwvi rlds rldscs rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs lwp cdnc cltc od550aer
# do
#     find . -name ${v}_*19[7-9]*01-19[7-9]*12.nc >> $flist
#     find . -name ${v}_*2*01-2*12.nc >> $flist
# done
# tar -cvf $tgt -T $flist

########## For Bob - monthly variables from 2005-2014 from hist
# for v in mmraerh2o mmrbc mmrdust mmrnh4 mmrno3 mmroa mmrpm1 mmrpm10 mmrpm2p5 mmrso4 mmrsoa mmrss o3 ps pr tas
# do
#     find . -name ${v}_A*200[5-9]01-200[5-9]12.nc >> $flist
#     find . -name ${v}_A*201[0-4]01-201[0-4]12.nc >> $flist
# done
# tar -cvf $tgt -T $flist

########## For Us - monthly variables from hist and pict
# for v in tas siconc mlotst
# do
#     find . -name ${v}_*mon_EC-Earth3-AerChem_*.nc >> $flist
# done
# tar -cvf $tgt -T $flist

########## For Gunnar - monthly variables from hist - all Amon or AERmon  (od550aer is also available from AERday) - 22 G and 4.2 G
# rm -f $flist-1 $flist-2
# for v in rsut rsutcs rsutaf rsutcsaf abs550aer od440aer od550aer od550aerh2o od550bc od550dust od550lt1aer od550no3 od550oa od550so4 od550soa od550ss od870aer
# do
#     find . -name ${v}_A*mon_EC-Earth3*.nc >> $flist-1
# done
# for v in mmraerh2o mmrbc mmrdust mmrnh4 mmrno3 mmroa mmrso4 mmrsoa mmrss
# do
#     find . -name ${v}_A*mon_EC-Earth3*.nc >> $flist-2
# done
# tar -cvf EC-Earth3-AerChem-$XPRMNT-$NAME-RAD.tar -T $flist-1
# tar -cvf EC-Earth3-AerChem-$XPRMNT-$NAME-MMR.tar -T $flist-2


########## For Pierre Nadat - monthly variables from hist and AMIP - all Amon or AERmon - Cannot provide tntrs
for v in od550aer abs550aer rsdt rsut rsds rsus clt rsutcs rsdscs rsuscs
do
    find . -name ${v}_A*mon_EC-Earth3*.nc >> $flist
done
tar -cvf $tgt -T $flist




#-1 tar -cvf $tgt $root/AERmon/mmraerh2o \
#-1     $root/AERmon/mmrbc \
#-1     $root/AERmon/mmrdust \
#-1     $root/AERmon/mmrnh4 \
#-1     $root/AERmon/mmrno3 \
#-1     $root/AERmon/mmroa \
#-1     $root/AERmon/mmrpm1 \
#-1     $root/AERmon/mmrpm10 \
#-1     $root/AERmon/mmrpm2p5 \
#-1     $root/AERmon/mmrso4 \
#-1     $root/AERmon/mmrsoa \
#-1     $root/AERmon/mmrss \
#-1     $root/AERmon/o3 \
#-1     $root/AERmon/ps \
#-1     $root/Amon/ps \
#-1     $root/Amon/tas \
#-1     $root/fx/areacella \
#-1     $root/fx/sftlf


# tar -cvf $tgt $root/AERmon/mmrss \
#     $root/AERmon/dms \
#     $root/Amon/cl \
#     $root/Amon/cli \
#     $root/Amon/clivi \
#     $root/Amon/clt \
#     $root/Amon/clw \
#     $root/Amon/clwvi \
#     $root/Amon/rlds \
#     $root/Amon/rldscs \
#     $root/Amon/rlus \
#     $root/Amon/rlut \
#     $root/Amon/rlutcs \
#     $root/Amon/rsds \
#     $root/Amon/rsdscs \
#     $root/Amon/rsdt \
#     $root/Amon/rsus \
#     $root/Amon/rsuscs \
#     $root/Amon/rsut \
#     $root/Amon/rsutcs \
#     $root/AERmon/lwp \
#     $root/AERmon/cdnc \
#     $root/AERmon/cltc

# From Emon, we also have
#
#  reffclws

#-2 tgt=EC-Earth3-AerChem-$XPRMNT-airmass.tar
#-2 tar -cvf $tgt $root/AERmon/airmass

#-3 tgt=EC-Earth3-AerChem-$XPRMNT-rad.tar
#-3 
#-3 echo
#-3 echo $datadir/$tgt
#-3 echo
#-3 
#-3 tar -cvf $tgt $root/Amon/rsut \
#-3     $root/Amon/rsdt \
#-3     $root/Amon/rlut
#-3 

mv $tgt /lustre3/projects/CMIP6/sager/cmorised-results/
