#!/bin/bash
#SBATCH --job-name=Amon-ps
#SBATCH --account=proj-aerchemmip
#SBATCH --nodes=1
#SBATCH --exclusive

set -eu

version=v20200904

usage() {
    cat << EOT >&2

Create missing Amon/ps data from CFday/ps or 3hr/ps data set

Usage: $(basename $0) [-v version] EXPERIMENT YSTART YEND

Options
   -v vYYYYMMDD : specify the version, default $version

EOT
}

# -- options
use3hr=

while getopts "h?v:" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        v) version=$OPTARG
    esac
done
shift $((OPTIND-1))

# -- Arg
if [ "$#" -ne 3 ]; then
    echo; echo " NEED THREE ARGUMENTS, NO MORE, NO LESS!"; echo
    usage
    exit 1
fi
 
EXP=$1
FIRST_YEAR=$2
LAST_YEAR=$3

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        usage
        exit 1
esac

if [[ $PLATFORM == 'RHINO' ]]
then
    datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/CMIP6
elif [[ $PLATFORM == 'CCA' ]]
then
    datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/CMIP6
fi

root=$datadir/${MIP}/EC-Earth-Consortium/EC-Earth3-AerChem/$XPRMNT/r1i1p1f1

# create top dir

topdir=$root/Amon/ps/gr/${version}
mkdir -p $topdir

t1=$(date +%s)

# workhorse
doit() {
    local YYYY=$1
    tgt=$topdir/ps_Amon_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YYYY}01-${YYYY}12.nc

    src=$root/CFday/ps/gr/${version}/ps_CFday_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YYYY}0101-${YYYY}1231.nc
    echo Try $src

    is3=0
    if [[ ! -f $src ]]
    then
        src=$root/3hr/ps/gr/${version}/ps_3hr_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_${YYYY}01010000-${YYYY}12312100.nc
        echo Try $src
        is3=1
    fi

    [[ ! -f $src ]] && echo "Did not find PS file for $YYYY" && return 1
    
    echo cdo monmean $src $tgt

    if (( is3 ))
    then
        ## Need some cleanup if using 3hr (and you must use the --write flag when calling nctxck from nctime)
        ## Tested/required with cdo 1.8.1
        \rm -f  $tgt.bak
        cdo --no_history monmean $src $tgt.bak

        cdo -O --no_history settbounds,'month' $tgt.bak $tgt
        ncatted -Oh -a cell_methods,ps,o,c,"area: time: mean" $tgt
        \rm -f  $tgt.bak
        echo Fix cdo-monmean-3
    else
        cdo --no_history monmean $src $tgt
    fi

    ncatted -Oh -a table_id,global,o,c,"Amon" $tgt
    ncatted -Oh -a tracking_id,global,o,c,"hdl:21.14100/$(uuidgen)" $tgt
}

export -f doit
export root topdir version XPRMNT 
seq $FIRST_YEAR $LAST_YEAR | parallel doit

t2=$(date +%s)
tr=$(date -d "0 -$t1 sec + $t2 sec" +%T)
echo $tr
