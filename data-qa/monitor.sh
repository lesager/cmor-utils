#!/bin/bash

date +%Y-%m-%d

#exp="a4co piNF hist pict"
exp="sst5 sst6 sst7 pic1 pic2 pic3"
#exp="s001 s002 s003"

(( $# )) && exp="$@"

source /nfs/home/users/sager/EC-Earth/utils/ecearth_functions.sh

for e in $exp
do
    echo
    echo "-----------------------------"
    echo "---------   $e   ------------"
    echo "-----------------------------"
    echo

    nstart=311
    
    nleg=$(cexp $e | sed -nr "s|leg_number=(.*)|\1|"p)

    if [[ ! $nleg =~ ^[0-9]+$ ]]
    then
        echo "*EE* No leg found for $e !!!"
        continue
    fi

    #[[ $e =~ sst ]] && nleg=86
    
    # IFS
    for k in $(eval echo {$nstart..$nleg}); do ./check-cmor-logs.sh ${e} ifs $k; done \
        | grep -v "No next job is launched.\|Unsupported combination of frequency monC with time operators \['mean within years', 'mean over years'\] encountered\|Skipped removal of nonempty work director" \
               | grep -v '!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Warning: Your input attribute "activity_id" with value 
! "RFMIP" will be replaced with value "RFMIP AerChemMIP"
! as defined for experiment_id "piClim-control".
!  See Control Vocabulary JSON file.(/nfs/home/users/sager/cmorize/ece2cmor3/ece2cmor3/resources/tables/CMIP6_CV.json)
!
!!!!!!!!!!!!!!!!!!!!!!!!!' | grep -v 'C Traceback:
In function: _CV_checkExperiment'
    echo
    
    # TM5
    for k in $(eval echo {$nstart..$nleg}); do ./check-cmor-logs.sh ${e} tm5 $k; done \
        | grep -v "No next job is launched.\|Could not find file containing data for variable co2\|Cmorizing failed for co2\|The source variable co2" \
               | grep -v '!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Warning: Your input attribute "activity_id" with value 
! "RFMIP" will be replaced with value "RFMIP AerChemMIP"
! as defined for experiment_id "piClim-control".
!  See Control Vocabulary JSON file.(/nfs/home/users/sager/cmorize/ece2cmor3/ece2cmor3/resources/tables/CMIP6_CV.json)
!
!!!!!!!!!!!!!!!!!!!!!!!!!' | grep -v 'C Traceback:
In function: _CV_checkExperiment'
    echo
    
    # NEMO
    if [[ ! $e =~ sst && ! $e =~ pic[1-3] ]] ; then 
        for k in $(eval echo {$nstart..$nleg}); do ./check-cmor-logs.sh ${e} nemo $k; done \
            | grep  -v "No next job is launched.\|Variable dissicnat            in table Oyr        was not found in the NEMO output files: task skipped.\|Variable talknat              in table Oyr        was not found in the NEMO output files: task skipped.\|The grid opa_grid_1point consists of a single point which is not supported, dismissing variables masso in Omon,volo in Omon,zostoga in Omon,thetaoga in Omon,bigthetaoga in Omon,tosga in Omon,soga in Omon,sosga in Omon\| zhalfo\| Dimension ncatice could not be found in file.*lim_grid_T_2D.nc, inserting using length-one dimension instead"
        echo
    fi
    
done
