#!/bin/bash

set +e

usage() {
    cat << EOT >&2

Usage: $(basename $0) [-v] EXPERIMENT MODEL LEG

Check the three logs of a cmorization job.
Options:
        -v : print the name of the files being checked

Examples:

   ./check-cmor-logs.sh -v hist ifs 1

   for k in {1..84}; do ./check-cmor-logs.sh piNF ifs \$k; done | grep -v "Unsupported combination of frequency monC with time operators \\['mean within years', 'mean over years'\\] encountered"

   for k in {1..84}; do ./check-cmor-logs.sh piNF nemo \$k; done | grep  -v "Variable dissicnat            in table Oyr        was not found in the NEMO output files: task skipped.\\|Variable talknat              in table Oyr        was not found in the NEMO output files: task skipped.\\|The grid opa_grid_1point consists of a single point which is not supported, dismissing variables masso in Omon,volo in Omon,zostoga in Omon,thetaoga in Omon,bigthetaoga in Omon,tosga in Omon,soga in Omon,sosga in Omon\\| zhalfo\\| Dimension ncatice could not be found in file.*lim_grid_T_2D.nc, inserting using length-one dimension instead"

EOT
}


# -- options
verbose=0

while getopts "h?v" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        v)  verbose=1
    esac
done
shift $((OPTIND-1))

if (( $# != 3 ))
then
    usage
    exit 1
fi

EXP=$1


# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    spin) XPRMNT=piControl ;            MIP=CMIP ;;
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

topdir=/lustre3/projects/CMIP6
[[ $XPRMNT =~ ssp370 ]] && topdir=/lustre2/projects/model_testing

logdir=$topdir/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/logs
OUT=/nfs/home/users/sager/cmorize/runtime/log

echo " -- $1-$2-$(printf %03d $3) --"

# --- submit script log
f=$OUT/$1/$1-$2-$(printf %03d $3).out
if [[ -f $f ]]
then
    (( verbose )) && ( echo; echo " -- Check $f --")
    grep -v "Submitted batch job\|Submit job for" $f
else
    echo "submit script ($f) log not found"
fi

# --- ece2cmor3 logs
cd $logdir

f=$(find . -name "$1-$2-$(printf %03d $3)-*cmor.log" -exec ls -1 {} \; | sort | tail -1)
if [[ -f $f ]]
then
    (( verbose )) && ( echo; echo " -- Check $f --")
    grep -v "^! ------$\|^! All files were closed successfully. $\|^! ------$\|^! $\|^$" $f
else
    echo 'CMOR log #1 not found'
fi

f=$(find . -name "$1-$2-$(printf %03d $3)-*[0-9].log" -exec ls -1 {} \; | sort | tail -1)
if [[ -f $f ]]
then
    (( verbose )) && ( echo; echo " -- Check $f --")
    grep -i "ERROR\|WARNING" $f
    # the following is ok for the first leg only
    grep "Assumed previous month file for.*ICM.*+000000" $f
else
    echo 'CMOR log #2 not found'
fi
