#!/bin/bash

#SBATCH --account=proj-aerchemmip
#SBATCH --nodes=1
#SBATCH --exclusive

usage()
{
    echo "Wrapper around QA/nctime"
    echo  Usage: $(basename $0) EXPERIMENT YEAR_START
}

if (( $# != 2 ))
then
    echo; echo "*EE* missing arguments"; echo
    usage; exit 1
fi

if [[ ! $1 =~ ^[a-zA-Z0-9_]{4}$ ]]
then
    echo; echo "*EE* argument EXP (=$1) should be a 4-letter string"; echo
    usage; exit 1
fi

if [[ ! $2 =~ ^[0-9]{4}$ ]]
then
    echo ;echo "*EE* argument YEAR_START (=$2) should be a 4-digit integer"; echo
    usage; exit 1
fi

exp=$1
ystart=$2

export PLATFORM='RHINO'

#exp=pict
#ystart=1850

source /nfs/home/users/sager/EC-Earth/utils/ecearth_functions.sh

echo
echo "---------   $exp   ------------"
echo

nleg=$(cexp $exp | sed -nr "s|leg_number=(.*)|\1|"p)

[[ $exp =~ sst ]] && nleg=86


if [[ ! $nleg =~ ^[0-9]+$ ]]
then
    echo *EE* No leg found for $exp use 165 as default !!!
    nleg=165
else
    echo *II* $nleg years simulated
fi

t1=$(date +%s)

# do (-1) or don't (-2) look at the last year
./check-cmor-output-files.sh -e -t  $exp $ystart $(( $ystart + $nleg -1))

t2=$(date +%s)
tr=$(date -d "0 -$t1 sec + $t2 sec" +%T)
echo $tr
