#!/bin/bash

set -eu

usage() {
    cat << EOT >&2

Implement data-qa/list-of-tests. Without options, only prints versions.

Usage: $(basename $0) [-e] [-t] [-v vYYYYMMDD] [-m] [-f] EXPERIMENT YSTART YEND

Options
   -e: remove empty files/dir and temporary files (dry-run unless -m is set)
   -t: run NCTIME tools (nctcck & nctxck). Should be submitted to a compute node.
   -v: set version (dry-run unless -m is set)
   -f: print number of files per year (only option that requires YSTART YEND actually)
   -m: if -v, effectively set version; if -e, remove empties and temporary files

EOT
}

# -- options
do_nctime=0
do_empty=0
do_fpy=0
opts=
del=0

while getopts "h?tv:mef" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        v) opts=${opts}" -v $OPTARG"
           ;;
        m) opts=${opts}" -m"
            del=1
           ;;
        e) do_empty=1 ;;
        t) do_nctime=1 ;;
        f) do_fpy=1
    esac
done
shift $((OPTIND-1))

# -- Arg
if [ "$#" -ne 3 ]; then
    echo; echo " NEED THREE ARGUMENTS, NO MORE, NO LESS!"; echo
    usage
    exit 1
fi
 
EXP=$1
FIRST_YEAR=$2
LAST_YEAR=$3

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem
# exception
[[ $EXP = 'xh2t' || $EXP = 's2hh' ]] && ECEMODEL=EC-EARTH3P-HR

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    hspr) XPRMNT=histSST-piAer ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    pic4) XPRMNT=piClim-aer ;     MIP=RFMIP ;;
    xh2t) XPRMNT=highres-future ; MIP=HighResMIP ;;
    s2hh) XPRMNT=highres-future ; MIP=HighResMIP ;;
    *)
        echo "Unknown experiment name: $EXP"
        #usage
        #exit 1
esac

if [[ $PLATFORM == 'RHINO' ]]
then
    datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/CMIP6
    TOPDIR=/nfs/home/users/sager/cmorize/ece2cmor3/ece2cmor3/scripts/data-qa # scripts distributed with ece2cmor3
elif [[ $PLATFORM == 'CCA' ]]
then
    datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/CMIP6
    TOPDIR=/perm/ms/nl/nm6/cmorize/ece2cmor3/ece2cmor3/scripts/data-qa # scripts distributed with ece2cmor3
elif [[ $PLATFORM = 'TEMS' ]]
then
    datadir=$SCRATCH/cmorized-results/$EXP/CMIP6
    #datadir=/ec/res4/scratch/nks/cmorized-results/$EXP/CMIP6/ #Twan [link is not enough for version]
    TOPDIR=$PERM/ecearth3/cmorize/ece2cmor3/ece2cmor3/scripts/data-qa # scripts distributed with ece2cmor3
fi

set +u

if (( do_empty ))
then
    echo "*II* Check for empty files/dir"
    (( del )) &&  find -L $datadir -empty -exec rmdir {} + || find -L $datadir -empty

    echo "*II* Check for auxiliary files"
    (( del )) && find -L $datadir -type f ! -name \*.nc -exec rm {} + || \
        find -L $datadir -type f ! -name \*.nc

    # temporary files from CMOR library
    (( del )) && find -L $datadir -name '*_r1i1p[0-9]f[0-9]_g[rn][a-zA-Z0-9]*' -exec rm {} + || \
        find -L $datadir -name '*_r1i1p[0-9]]f[0-9]_g[rn][a-zA-Z0-9]*'
    #find -L . -type f -regextype sed ! -regex '.*/.*_EC-Earth3-AerChem_[0-9a-zA-Z]*_r1i1p1f1_g[nr][_0-9-]*\.nc'
fi

if [[ ${opts} =~ v ]]
then
    # versions.sh -v vYYYYMMDD . # set version, dry-run)  
    # versions.sh -v vYYYYMMDD -m . # set version
    echo "*II* FIX VERSION"
    $TOPDIR/scripts/versions.sh $opts $datadir
    exit $?
else
    echo "*II* List versions" # If need fixing, do it afterwards
    $TOPDIR/scripts/versions.sh -l $datadir
fi

if (( do_fpy ))
then
    #echo "*II* Number of files per year"
    $TOPDIR/scripts/files-per-year.sh $FIRST_YEAR $LAST_YEAR $datadir

    # Check number of files per variable:  
    #$TOPDIR/scripts/files-per-variable <VERSION> <VAR_DIRS>
fi

# Fix "sftof_Ofx" files
#$TOPDIR/scripts/sftof.py CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3/historical/r1i1p1f1/Ofx/sftof/gn/v20190926/sftof_Ofx_

if (( do_nctime ))
then
    echo "*II* NCTIME check"

    # nctime was installed in the ece2cmor3 env
    if [[ $PLATFORM == 'RHINO' ]]
    then
        . /lustre2/projects/model_testing/sager/miniconda2/etc/profile.d/conda.sh
    elif [[ $PLATFORM == 'CCA' ]]
    then
        . /scratch/ms/nl/nm6/miniconda2/etc/profile.d/conda.sh
    elif [[ $PLATFORM = 'TEMS' ]]
    then
        echo " No available yet: relies on python 2.6+, need dedicated installation"
        exit
        #. $HPCPERM/mamba/etc/profile.d/conda.sh
    fi
    conda activate ece2cmor3

    cfgdir=$TOPDIR/nctime/esgini-dir
    nctxck -p cmip6 -i $cfgdir --max-processes -1 -l log $datadir > /dev/null
    nctcck -p cmip6 -i $cfgdir --max-processes -1 -l log $datadir > /dev/null
fi
