#!/bin/bash
#
#SBATCH --partition=all
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=28
#SBATCH --account=proj-aerchemmip
#
# Some possible accounts:  proj-cmip6, model-testing, proj-aerchemMIP

usage()
{
    echo
    echo " #####################################"
    echo " # Run cmor-fixer for ONE experiment #"
    echo " #####################################"
    echo
    echo " SUBMIT"
    echo "    sbatch [--job-name=<JOBNAME>] fix-cmor.sh <EXP>"
    echo
    echo " EXAMPLES (they assume that the --output dir exists!)"
    echo
    echo "    sbatch --output=log/fixcmor-a4co.out --job-name=cf-a4co fix-cmor.sh a4co"
    echo
}

error() {
    echo; echo "ERROR: $1" >&2
    exit 1
}

set -e

if [ "$#" -eq 1 ]; then

    EXP=$1
    
    # Common Experiment Def
    ECEMODEL=EC-EARTH-AerChem
    PEXTRA='-pextra'

    # Specific Experiment Def
    case $EXP in
        hist) XPRMNT=historical ;           MIP=CMIP ;;
        pict) XPRMNT=piControl ;            MIP=CMIP ;;
        a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
        piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
        s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
        s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
        s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
        sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
        sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
        sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
        pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
        pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
        pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
        *)
            echo "wrong exp: $EXP"
            usage
            exit 1
    esac

    # Experiment control output setup
    if [[ $MIP = 'CMIP' ]]
    then
        CTRLDIR=/lustre3/projects/AerChemMIP/sager/submitdirs/${XPRMNT}/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/${ECEMODEL}/cmip6-experiment-${MIP}-${XPRMNT}
    else
        CTRLDIR=/lustre3/projects/AerChemMIP/sager/submitdirs/${XPRMNT}/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/cmip6-experiment-${MIP}-${XPRMNT}
    fi
    METADATA=${CTRLDIR}/metadata-cmip6-${MIP}-${XPRMNT}-${ECEMODEL}-$COMPONENT-template.json
    
    # CMOR output
    ODIR=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP

    #ODIR=/lustre3/projects/CMIP6/sager/cmorised-results/9bad-original
    
    [[ ! -d $ODIR ]] && error "Directory $ODIR does not exist"
    [[ -z $(ls -A $ODIR) ]] && error "Empty dir: $ODIR"

    . /lustre2/projects/model_testing/sager/miniconda3/etc/profile.d/conda.sh
    conda activate cmorfixer

    export HDF5_USE_FILE_LOCKING=FALSE

    cd /nfs/home/users/sager/cmorize/cmor-fixer
    ./cmor-fixer.py --verbose --keepid --olist --npp 28 $ODIR
    
else
    usage
    error '  Illegal number of arguments: the script requires three arguments!'
fi
