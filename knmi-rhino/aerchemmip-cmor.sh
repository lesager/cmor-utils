#!/bin/bash
#
#SBATCH --partition=all
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=28
#SBATCH --account=proj-aerchemmip
#
# Some possible accounts:  proj-cmip6, model-testing, proj-aerchemMIP

usage()
{
    cat <<EOT >&2

 ######################################################################################
 # CMORISE EC-Earth3 raw output from ONE experiment, ONE leg, and ONE model component #
 ######################################################################################

 SUBMIT
    sbatch [--job-name=<JOBNAME>] aerchemmip-cmor.sh <EXP> <COMPONENT> <LEG>

 EXAMPLES (they assume that the --output dir exists!)

    sbatch --output=log/caer-nemo-001.out --job-name=caer-nemo-001 aerchemmip-cmor.sh caer nemo 1

  or (one exp, one model, several legs), but no more than 4 since n+4 is submitted:

    exp=hist; model=ifs ; for i in {1..4}; do mess=\${exp}-\${model}-\$(printf %03d \$i); sbatch --output=log/\$exp/\${mess}.out --job-name=\$mess aerchemmip-cmor.sh \${exp} \${model} \$i; done

  or (one exp, one leg, all models):

    exp=a4co; for model in ifs nemo tm5; do mess=\${exp}-\${model}-001; sbatch --output=log/\${mess}.out --job-name=\$mess aerchemmip-cmor.sh \${exp} \${model} 1; done

EOT
}

error() {
    echo; echo "ERROR: $1" >&2
    exit 1
}

# ECEDIR    directory with the raw ec-earth output results, for instance: t001/output/nemo/001
# ECEMODEL  name of the ec-earth model configuration, for instance: EC-EARTH-AOGCM
# METADATA  name of the meta data file, for instance: ece2cmor3/resources/metadata-templates/cmip6-CMIP-piControl-metadata-template.json
# VARSLIST  name of the variable list, in this case the so called json cmip6 data request file, for instance: cmip6-data-request-varlist-CMIP-piControl-EC-EARTH-AOGCM.json
# TEMPDIR   directory where ece2cmor3 is writting files during its execution
# ODIR      directory where ece2cmor3 will write the cmorised results of this job
# COMPONENT name of the model component for the current job to cmorise
# LEG       leg number for the current job to cmorise. Note for instance leg number one is written as 001.

set -e

#echo "PYTHONPATH: $PYTHONPATH"
#echo "PATH:"
#echo $PATH | tr ":" "\012"
#echo "-------------------"

if [ "$#" -eq 3 ]; then

    EXP=$1
    COMPONENT=$2
    ILEG=$3
    LEG=$(printf %03d $ILEG)
    
    # Common Experiment Def
    ECEMODEL=EC-EARTH-AerChem
    PEXTRA='-pextra'

    # Specific Experiment Def
    case $EXP in
        spin) XPRMNT=piControl ;            MIP=CMIP ;;
        hist) XPRMNT=historical ;           MIP=CMIP ;;
        pict) XPRMNT=piControl ;            MIP=CMIP ;;
        a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
        piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
        s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
        s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
        s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
        sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
        sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
        sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
        pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
        pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
        pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
        *)
            echo "wrong exp: $EXP"
            exit 1
    esac

    # Experiment control output setup
    if [[ $MIP = 'CMIP' || $MIP = 'ScenarioMIP' ]]
    then
        CTRLDIR=/lustre3/projects/AerChemMIP/sager/submitdirs/${XPRMNT}/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/${ECEMODEL}/cmip6-experiment-${MIP}-${XPRMNT}
    else
        CTRLDIR=/lustre3/projects/AerChemMIP/sager/submitdirs/${XPRMNT}/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/cmip6-experiment-${MIP}-${XPRMNT}
    fi
    METADATA=${CTRLDIR}/metadata-cmip6-${MIP}-${XPRMNT}-${ECEMODEL}-$COMPONENT-template.json
    VARSLIST=${CTRLDIR}/cmip6-data-request-varlist-${MIP}-${XPRMNT}-${ECEMODEL}.json
    
    # Model output
    [[ $XPRMNT =~ ssp370SST ]] && RESROOT=/lustre2/projects/model_testing || RESROOT=/lustre3/projects/AerChemMIP
    ECEDIR=$RESROOT/sager/rundirs/$EXP/output/$COMPONENT/$LEG
    [[ ! -d $ECEDIR ]] && error "Directory $ECEDIR does not exist"
    [[ -z $(ls -A $ECEDIR) ]] && error "Empty dir: $ECEDIR"

    # CMOR output
    topdir=/lustre3/projects/CMIP6
    [[ $XPRMNT =~ ssp370 ]] && topdir=/lustre2/projects/model_testing
    
    TEMPDIR=${topdir}/sager/temp-cmor-dir/$EXP/$COMPONENT/$LEG
    ODIR=${topdir}/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
    mkdir -p $ODIR
    if [ -d $TEMPDIR ]; then rm -rf $TEMPDIR; fi
    mkdir -p $TEMPDIR

    . /lustre2/projects/model_testing/sager/miniconda2/etc/profile.d/conda.sh
    conda activate ece2cmor3

    export HDF5_USE_FILE_LOCKING=FALSE
    export UVCDAT_ANONYMOUS_LOG=false

    ece2cmor $ECEDIR --exp       $EXP      \
             --ececonf           $ECEMODEL \
             --$COMPONENT                  \
             --meta              $METADATA \
             --varlist           $VARSLIST \
             --tmpdir            $TEMPDIR  \
             --odir              $ODIR     \
             --npp               28        \
             --overwritemode     replace   \
             --log
    
    mkdir -p $ODIR/logs
    mv -f $EXP-$COMPONENT-$LEG-*.log $ODIR/logs/
    if [ -d $TEMPDIR ]; then rm -rf $TEMPDIR; fi

    #ON-THE-EDGE # Leaving on the edge - remove N-1 leg output if CMOR of N and N-1 succeeded 
    #ON-THE-EDGE if (( ILEG > 2 ))
    #ON-THE-EDGE then
    #ON-THE-EDGE     chckf=/nfs/home/users/sager/cmorize/runtime/${EXP}-${COMPONENT}-$LEG.txt
    #ON-THE-EDGE     touch $chckf
    #ON-THE-EDGE     went_wrong=0
    #ON-THE-EDGE     PREV=$(( ILEG -1 ))
    #ON-THE-EDGE 
    #ON-THE-EDGE     # Intermediate logs are removed upon success
    #ON-THE-EDGE     for f in /nfs/home/users/sager/cmorize/runtime/${EXP}-${COMPONENT}-$(printf %03d $PREV)-*.log \
    #ON-THE-EDGE              /nfs/home/users/sager/cmorize/runtime/${EXP}-${COMPONENT}-$LEG-*.log                                  
    #ON-THE-EDGE     do
    #ON-THE-EDGE         if [[ -f $f ]] ; then
    #ON-THE-EDGE             went_wrong=1
    #ON-THE-EDGE             echo "Won't remove $(printf %03d $PREV), since $f exists" >> $chckf
    #ON-THE-EDGE         fi
    #ON-THE-EDGE     done
    #ON-THE-EDGE 
    #ON-THE-EDGE      # Final log should be empty (weak test, since it will pass when N and N-1 are being cmorized, but strong once these cmorizations are done) 
    #ON-THE-EDGE     leglog1=/nfs/home/users/sager/cmorize/runtime/log/${EXP}/${EXP}-${COMPONENT}-$(printf %03d $PREV).out
    #ON-THE-EDGE     leglogN=/nfs/home/users/sager/cmorize/runtime/log/${EXP}/${EXP}-${COMPONENT}-$LEG.out
    #ON-THE-EDGE     
    #ON-THE-EDGE     [[ ! -f $leglog1 || -s $leglog1 ]] && went_wrong=1
    #ON-THE-EDGE     [[ ! -f $leglogN || -s $leglogN ]] && went_wrong=1
    #ON-THE-EDGE 
    #ON-THE-EDGE     if (( ! went_wrong ))
    #ON-THE-EDGE     then
    #ON-THE-EDGE         cd $RESROOT/sager/rundirs/$EXP/output/$COMPONENT/$(printf %03d $PREV)
    #ON-THE-EDGE         echo "Removing $(printf %03d $PREV)" >> $chckf
    #ON-THE-EDGE         for fff in *
    #ON-THE-EDGE         do
    #ON-THE-EDGE             [[ -f $fff ]] && rm -f $fff
    #ON-THE-EDGE         done
    #ON-THE-EDGE     fi
    #ON-THE-EDGE fi
    
else
    usage
    error '  Illegal number of arguments: the script requires three arguments!'
fi
