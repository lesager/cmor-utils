#!/usr/bin/env bash

set -eu

(( $# != 1 )) && echo NEED EXP ARGUMENT && exit 1

EXP=$1

# Switch
#NON="NON-"
NON=

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP

src=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/NON-CMIP6
if [[ -n $NON ]]
then
    datadir=/lustre2/projects/model_testing/sager/aerchemip/$EXP
    mkdir -p $datadir
fi

file_list="file.list."$NON$EXP
checksum_file="sha256sums-"$NON$EXP

sbatch <<EOF
#!/bin/bash
#SBATCH --job-name=csum-$NON$EXP
#SBATCH --account=proj-aerchemmip
#SBATCH --nodes=1
#SBATCH --output="log/checksum-"$NON$EXP".log"
#SBATCH --exclusive

# Uses GNU parallel for parallelization of checksum computation. Make sure it's
# available! (on NSC, load module parallel/...)

####rsync -av $src $datadir

cd $datadir

find ${NON}CMIP6 -type f > $file_list

/usr/bin/time parallel -a $file_list sha256sum > $checksum_file

#####tar -cvf NONCMIP6-AerChem-${XPRMNT}.tar NON-CMIP6 $checksum_file $file_list

EOF
