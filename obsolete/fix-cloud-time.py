#!/usr/bin/env python
"""
# Remove small negatives from Liquid Cloud Time (see #743 EC-Earth3 proj) 
# -----------------------------------------------------------------------
#
"""

from __future__ import print_function
import sys
import argparse

# To avoid modifying PYTHONPATH (can quickly mess up conda envs) -
# Note that library has been installed with ~/anaconda2/bin/python, so
# this script needs to be called with that python!
sys.path.append("/nfs/home/users/sager/installed/eccodes/lib/python2.7/site-packages")

from eccodes import *
import numpy as np
import os

def main():
    parser = argparse.ArgumentParser(description='Remove small negatives in liquid cloud time, backup input file')
    parser.add_argument('input', help='ICMGG IFS to process')
    #parser.add_argument('output', help='New IFS output file')
    args = parser.parse_args()

    #os.environ['GRIB_DEFINITION_PATH'] = '/nfs/home/users/sager/EC-Earth/utils/grib_table_126-eccodes:/nfs/home/users/sager/installed/eccodes/share/grib_api/definitions'
    os.environ['ECCODES_DEFINITION_PATH'] = '/nfs/home/users/sager/EC-Earth/utils/grib_table_126-eccodes:/nfs/home/users/sager/installed/eccodes/share/eccodes/definitions'

    # backup
    bckp = args.input+'-BCKP'
    os.rename(args.input, bckp)

    fin = open(bckp, 'rb')
    fout = open(args.input, 'wb')

    while 1:
        gid = codes_grib_new_from_file(fin)
        if gid is None:
            break

        paramid = codes_get(gid, 'paramId')
        if paramid == 126022:
            dt = np.array(codes_get_values(gid))
            zeros = np.zeros_like(dt)
            updt = np.where(dt > 0.000001, dt, zeros)
            codes_set_values(gid, updt)
        codes_write(gid, fout)
        codes_release(gid)

    fin.close()
    fout.close()

if __name__ == '__main__':
    sys.exit(main())
