#!/bin/bash
#
#SBATCH --partition=hdd
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --account=proj-cmip6
##SBATCH --ntasks=12

# Submit
#
#    sbatch --output=log/caer-negative-001.out pre-process-ifs-gg.sh caer 1

set -e

EXP=$1
ILEG=$2

LEG=$(printf %03d $ILEG)

# Model output
ECEDIR=/lustre3/projects/AerChemMIP/sager/rundirs/${EXP}/output/ifs/$LEG

# Script to remove small negative Liquid Droplet Cloud Time
fixit=/nfs/home/users/sager/cmorize/runtime/fix-cloud-time.py

# Get to work
cd $ECEDIR

ls -lt

t1=$(date +%s)

for mm in ICMGG${EXP}+??????
do
    [[ $mm =~ 000000 ]] && continue
    #(echo $mm; sleep 4) &
    ($fixit $mm) &
    
done
wait

t2=$(date +%s)
tr=$(date -d "0 -$t1 sec + $t2 sec" +%T)
echo "# Finished after ${tr} (hh:mm:ss)"
