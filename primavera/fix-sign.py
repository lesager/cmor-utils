#!/usr/bin/env python3

import os
import shutil
import subprocess
import uuid
import glob, sys
import argparse
import errno
import logging
import numpy as np
import time
from netCDF4 import Dataset

ERRA=''' Switch sign of some variables '''

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=ERRA)
    parser.add_argument('-d', '--dryrun', help='dry-run', action='store_true')
    parser.add_argument('exp', help='variable name to consider')

    # All variables to manipulate if True
    var = [ ('rlut',    True),
                        ('rsus',    True),
                        ('rlus',    True),
                        ('rsut',    True),
                        ('rlutcs',  True),
                        ('rsuscs',  True),
                        ('rluscs',  True),
                        ('rsutcs',  True) ]

    args = parser.parse_args()
    try:
        NNN = [i[0] for i in var].index(args.exp)
    except ValueError:
        print("Unknown variable: {}".format(args.exp))
        sys.exit(1)

    v, doit = var[NNN]

    logging.basicConfig(filename='non-cmip6-{}.log'.format(v),
                        format='%(levelname)s:%(message)s', level=logging.INFO)
    logging.info(time.strftime("--- %Y-%m-%d %H:%M:%S %Z ---"))


    if not doit:
        logging.info("Skip {}".format(v))
        sys.exit(0)
    else:
        logging.info("Process {}".format(v))

        #   1st part of path from own script around ece2cmor3
        rootdata="/gws/nopw/j04/primavera2/upload/EC-Earth-Consortium/highresSST-present_upwelling"
        print(rootdata)

        #   2nd part from CMORization starts with
        srcdir = rootdata + "/CMIP6"
        tgtdir = rootdata + "/FIXED-CMIP6"

        # find all files for that variable
        for root,dirs,files in os.walk(os.path.expanduser(srcdir)):
            for f in files:
                if v+'_' in f:
                    src = os.path.join(root,f)
                    newroot = root.replace(srcdir, tgtdir)
                    mkdir_p(newroot)
                    tgt = os.path.join(newroot,f)
                    if os.path.isfile(tgt):
                        #raise RuntimeError('Tgt file "{}" already exists!'.format(tgt))
                        logging.info("{}".format(src.replace(rootdata,'Skip: ')))
                        print("{}".format(src.replace(rootdata,'Skip: ')))
                    else:
                        shutil.copy2(src=src, dst=tgt)

                        #logging.info("Process:\n  {}\n  {}".format(src,tgt))
                        logging.info("{}".format(src.replace(rootdata,'')))
                        print("{}".format(src.replace(rootdata,'')))

                        dvar = Dataset(tgt, 'r+')
                        try:
                            dat = np.array(dvar.variables[v][:])
                            data = (-1.0) * dat
                        except KeyError:
                            raise RuntimeError('Variable "{}" not found in "{}"!'.format(v, src))

                        dvar.variables[v][:] = data
                        dvar.tracking_id = 'hdl:21.14100/' + str(uuid.uuid4())
                        dvar.history = dvar.history + time.strftime(", %Y-%m-%dT%H:%M:%S%Z SignFlip")
                        dvar.close()

                        # test one is enough
                        if args.dryrun:
                            sys.exit(0)

