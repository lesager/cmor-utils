#!/usr/bin/env bash

#### Turns out to be a lot faster than the python script

set -eu

(( $# != 1 )) && echo NEED ONE ARGUMENT && exit 1
v=$1

# All possible variables
pvar=' rlut rsus rlus rsut rlutcs rsuscs rluscs rsutcs '

if [[ ! $pvar =~ " $v " ]]
then
    echo "Unexpected variable $v"
    exit 1
fi

log=$HOME/cdo-fix-${v}.log

rootdata="/gws/nopw/j04/primavera2/upload/EC-Earth-Consortium/highresSST-present_upwelling"

srcdir=CMIP6
tgtdir=FIXED-CMIP6

# find all files for that variable

cd $rootdata

fff=$(find ${srcdir} -type f -name "${v}_*.nc")

for f in $fff
do
    tgt=${f/$srcdir/$tgtdir}

    if [[ -e $tgt ]]
    then
        echo "SKIP: "$f >> $log
        echo "SKIP: "$f
    else
        echo "PROC: "$f >> $log
        echo "PROC: "$f
        mkdir -p $(dirname $tgt)

        cdo -z zip --no_history mulc,-1.0 $f $tgt

        now=$(date "+%FT%H:%M:%S%Z SignFlip")
        id='hdl:21.14100/'$(uuid)
        ncatted -h -O \
            -a history,global,a,c,"$now" \
            -a tracking_id,global,o,c,$id $tgt

        # uncomment for testrun
        #exit 0
    fi
done

echo SUCCESS
