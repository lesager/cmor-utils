#!/usr/bin/env bash

usage() {
    cat << EOT >&2
Usage: ${0##*/} [-a account] [-c] [-l] [-v] <EXP> <COMPONENT> <LEG>

Submit a job to CMORISE EC-Earth3 raw output from ONE experiment, ONE leg, and ONE model component
Four AerChemMIP experiments are known: hist, pict, piNF, a4co, onep

Options are:
  -a account  : specify a different special project for accounting (default ${ECE3_POSTPROC_ACCOUNT:-DEFAULT})
  -c          : check for success of previously submitted script
  -l          : page the log files of previously submitted script with a '${PAGER:-less} <log>' command
  -v          : verbose, i.e. print the name of the files being checked (affects -c only)
  -d jobid    : submit after successful completion of jobid

Submit several jobs (one exp, one model, several legs):

   exp=onep; model=ifs ; for i in {1..10}; do sub-cmor.sh \${exp} \${model} \$i; done

 with dependency:

   rrrr=\$(sub-cmor.sh onep ifs 11)
   for ((x = 15 ; x <= 66 ; x += 4)); do rrrr=\$(sub-cmor.sh -d ${rrrr} onep ifs \${x}); done

Checking older jobs:

   # for ifs
   for k in {1..66}; do sub-cmor.sh -c onep ifs \$k ; done | grep -v "Unsupported combination of frequency monC\|ifs2cmor: Skipped removal of nonempty work directory"

   # for tm5 and nemo
   exp=onep; for model in nemo tm5; do for i in {11..66}; do sub-cmor.sh -c \${exp} \${model} \$i; done; done | grep -v "Variable dissicnat            in table Oyr        was not found in the NEMO output files: task skipped.\|Variable talknat              in table Oyr        was not found in the NEMO output files: task skipped.\|The grid opa_grid_1point consists of a single point which is not supported, dismissing variables masso in Omon,volo in Omon,zostoga in Omon,thetaoga in Omon,bigthetaoga in Omon,tosga in Omon,soga in Omon,sosga in Omon"

EOT
}

set -e

# -- options
account=$ECE3_POSTPROC_ACCOUNT
verbose=0
dependency=

while getopts "h?vca:d:" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        a)  account=$OPTARG
            ;;
        c)  chck=1
            ;;
        d)  dependency=$OPTARG
            ;;
        v)  verbose=1
            ;;
        l)  page=1
    esac
done
shift $((OPTIND-1))


# -- Arg
if [ "$#" -ne 3 ]; then
    echo; echo " NEED THREE ARGUMENTS, NO MORE, NO LESS!"; echo
    usage
    exit 0
fi

EXP=$1

# location of script to be submitted and its log
OUT=$SCRATCH/tmp_cmor
(( $chck )) || mkdir -p $OUT/log


# -- basic check
if (( $chck ))
then
    echo " -- $1-$2-$(printf %03d $3) --"
    # - submit script log
    f=$OUT/log/cmor_$1_$2_$3.out
    if [[ -f $f ]]
    then
        (( verbose )) && ( echo; echo " -- Check $f --")
        grep "## INFO Exit Code" $f
        status=$(sed -nr "s|## INFO Exit Code *: ||"p $f)
    else
        echo 'submit script log not found'
    fi

    # - ece2cmor3 logs
    # faster (hardcoded path):
    case $EXP in
        onep) XPRMNT=1pctCO2 ;        ECEMODEL=EC-EARTH-AerChem; MIP=CMIP ;;
        amip) XPRMNT=amip ;           ECEMODEL=EC-EARTH-AerChem; MIP=CMIP ;;
        s004) XPRMNT=ssp370pdSST ;    ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
        hpae) XPRMNT=hist-piAer ;     ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
        hsp4) XPRMNT=histSST-piCH4 ;  ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
        hspr) XPRMNT=histSST-piAer ;  ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
        xh2t) XPRMNT=highres-future ; ECEMODEL=EC-EARTH3P-HR;    MIP=HighResMIP ;;
        s2hh) XPRMNT=highres-future ; ECEMODEL=EC-EARTH3P-HR;    MIP=HighResMIP ;;
        hi1y) XPRMNT=historical ;     ECEMODEL=EC-EARTH-HR;      MIP=CMIP ;;
        pic4) XPRMNT=piClim-aer ;     ECEMODEL=EC-EARTH-AerChem; MIP=RFMIP ;;
        *) echo "wrong exp: $EXP"
            exit 1
    esac

    cd /scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/logs
    if [[ $status -ne 0 ]]; then
        cd /scratch/ms/nl/nm6/tmp_cmor
    fi
    (( verbose )) && ( echo; echo " -- Moved to $PWD --")
    
    set +e
    f=$(find . -name "$1-$2-$(printf %03d $3)-*cmor.log" -exec ls -1 {} \; | sort | tail -1)
    if [[ -f $f ]]
    then
        (( verbose )) && ( echo; echo " -- Check $f --")
        grep -v "^! ------$\|^! All files were closed successfully. $\|^! ------$\|^! $\|^$" $f
    else
        echo 'ECE2CMOR log not found'
    fi

    f=$(find . -name "$1-$2-$(printf %03d $3)-*[0-9].log" -exec ls -1 {} \; | sort | tail -1)
    if [[ -f $f ]]
    then
        (( verbose )) && ( echo; echo " -- Check $f --")
        grep -i "ERROR\|WARNING" $f
        grep "Assumed previous month file for.*ICM.*+000000" $f
    else
        echo 'CMOR log not found'
    fi

    exit
fi

# -- read log
if (( $page ))
then
    echo "Checking $OUT/log/cmor_$1_$2_$3.out"
    ${PAGER:-less} $OUT/log/cmor_$1_$2_$3.out
    exit
fi

# -- submit script
tgt_script=$OUT/cmor_$1_$2_$3

sed "s/<EXPID>/$1/" < aerchemmip-cmor.sh.tmpl > $tgt_script

[[ -n $account ]] && \
    sed -i "s/<ACCOUNT>/$account/" $tgt_script || \
    sed -i "/<ACCOUNT>/ d" $tgt_script

[[ -n $dependency ]] && \
    sed -i "s/<DEPENDENCY>/$dependency/" $tgt_script || \
    sed -i "/<DEPENDENCY>/ d" $tgt_script

sed -i "s|<CMP>|$2|" $tgt_script
sed -i "s|<LEG>|$3|" $tgt_script

cd $OUT
qsub $tgt_script
