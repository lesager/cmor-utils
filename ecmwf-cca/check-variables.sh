#! /usr/bin/env bash

set -eu

cd /scratch/ms/nl/nm6/cmorised-results/EC-EARTH-AerChem-CMIP-1pctCO2/onep/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-AerChem/1pctCO2/r1i1p1f1

# 1850 seems ok - use it as a reference
vars=$(find  . -name '*_1850*.nc' | sed 's:.*/\(.*\)_EC-Earth3-AerChem_1pctCO2_r1i1p1f1_g[nr]_1850.*\.nc:\1:' | sort)

for yyyy in 1853 1860 1892
do 
    news=$(find  . -name '*_'"$yyyy"'*.nc' | sed 's:.*/\(.*\)_EC-Earth3-AerChem_1pctCO2_r1i1p1f1_g[nr]_'"$yyyy"'.*\.nc:\1:' | sort)

    printf  "\n\t$yyyy\n"
    echo; echo "Extra variables in $yyyy:"
    comm -13 <(echo $vars | tr ' ' '\012') <(echo $news | tr ' ' '\012')
    echo; echo "Missing variables in $yyyy:"
    comm -23 <(echo $vars | tr ' ' '\012') <(echo $news | tr ' ' '\012')
    
done


