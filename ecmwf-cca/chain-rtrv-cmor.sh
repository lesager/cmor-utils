#!/bin/bash

set -e

usage() {
    cat << EOT >&2

 Usage:
        ${0##*/} [-d jobid] [-n] EXP NSTART NSTOP

 Submit jobs to retrieve, cmorize, and delete output of 
  legs NSTART to NSTOP of experiment EXP.

 Jobs are chained with the correct dependencies.

 Returns the jobid of the last IFS cmorization, so you
  can go meta. For example, by group of four:

   >>> jjj=\$(chain-rtrv-cmor.sh -n hpae 7 10)
   >>> jid=\${jjj%%,*}
   >>> for k in \$(seq 11 4 30); do jid=\$(chain-rtrv-cmor.sh -n -d \${jid} hpae \${k} \$((k+3))); done
 
 Options are:
    -n          : retrieve and cmorize NEMO output, default is IFS and TM5 only.
    -d depend   : add a job dependency to all retrievals.

EOT
}

# -- Options
with_nemo=0
rtrvdep=

while getopts "h?nd:" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        d)  rtrvdep=$OPTARG
            ;;
        n)  with_nemo=1
    esac
done
shift $((OPTIND-1))


# -- Arg
(( $# != 3 )) && usage && exit 1

EXP=$1
NSTART=$2
NSTOP=$3

# -- Utils
retrieve () {
    # Get leg N output from ECFS 
    cd /home/ms/nl/nm6/ECEARTH/postproc/ece3-raw-backup >/dev/null
    (( with_nemo )) && \
        dr=$(sub_ece3_rtrv.sh ${rtrvdep:+ -d $rtrvdep} -o tm5 -o ifs -o nemo $EXP $N) || \
        dr=$(sub_ece3_rtrv.sh ${rtrvdep:+ -d $rtrvdep} -o tm5 -o ifs $EXP $N)
    echo $dr
}

cmorize () {
    # CMORIZE leg N once N and N-1 data are available
    cd /home/ms/nl/nm6/ECEARTH/proj/ece2cmor >/dev/null
    dc=$(sub-cmor.sh -d ${depget_Nm1}${depget_Nm1:+,afterok:}${depget_N} $EXP ifs $N)
    dc=$dc,afterok:$(sub-cmor.sh -d ${depget_N} $EXP tm5 $N)
    (( with_nemo )) && dc=$dc,afterok:$(sub-cmor.sh -d ${depget_N} $EXP nemo $N)
    echo $dc
}

odelete () {
    # Remove N-1 output 
    prev=$(( N-1 ))
    cd /home/ms/nl/nm6/ECEARTH/postproc/ece3-raw-backup >/dev/null
    sub_ece3_del.sh -d $depcmo_N${depcmo_Nm1:+,afterok:}${depcmo_Nm1} -o tm5 -o ifs -o nemo $EXP $prev
}

# -- CHAIN REACTION (if starting from N /= 1, output from leg 1 and NSTART-1 should be available beforehand)

for N in $(eval echo {${NSTART}..${NSTOP}})
do
    depget_N=$(retrieve)
    depcmo_N=$(cmorize)

    # fork delete of N-1 data if possible
    (( $N > 2 )) && odelete > /dev/null
    
    depget_Nm1=$depget_N
    depcmo_Nm1=$depcmo_N
done

# Return last IFS cmor jobid
echo ${depcmo_N%%,*}
