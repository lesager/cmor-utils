#!/bin/bash

# Has to be run in this directory. For example:
# 
#      cd /home/ms/nl/nm6/ECEARTH/proj/ece2cmor
#      log=/home/ms/nl/nm6/ecearth3/diag/aerchem-$(date +%F).log
#      ./monitor.sh > $log


date +%Y-%m-%d

exp="hspr"

(( $# )) && exp="$@"

source /home/ms/nl/nm6/ECEARTH/utils/ecearth_functions.sh

for e in $exp
do
    echo
    echo "-----------------------------"
    echo "---------   $e   ------------"
    echo "-----------------------------"
    echo

    nleg=$(cexp $e | sed -nr "s|leg_number=(.*)|\1|"p)

    if [[ ! $nleg =~ ^[0-9]+$ ]]
    then
        echo "*EE* No leg found for $e !!!"
        continue
    fi

    nstart=1
    [[ $e = amip ]] && nstart=20  # skip the spinup years
    [[ $e = xh2t ]] && nstart=102 # extension started at 102
 
    # IFS
    for k in $(eval echo {$nstart..$nleg}); do ./sub-cmor.sh -c ${e} ifs $k ; done \
        | grep -v "Unsupported combination of frequency monC\|ifs2cmor: Skipped removal of nonempty work directory\|## INFO Exit Code                 : 0"
#        | grep -v "No next job is launched.\|Unsupported combination of frequency monC with time operators \['mean within years', 'mean over years'\] encountered\|Skipped removal of nonempty work director"
    echo
    # TM5
    [[ ! $e = xh2t ]] && [[ ! $e = s2hh ]] && \
        for k in $(eval echo {$nstart..$nleg}); do ./sub-cmor.sh -c  ${e} tm5 $k; done \
        | grep -v "No next job is launched.\|Could not find file containing data for variable co2\|Cmorizing failed for co2\|The source variable co2\|## INFO Exit Code                 : 0"
    echo
    # NEMO
    [[ $e = onep || $e = hpae || $e = xh2t || $e = s2hh ]] && \
        for k in $(eval echo {$nstart..$nleg}); do ./sub-cmor.sh -c ${e} nemo $k; done \
        | grep  -v "## INFO Exit Code                 : 0\|Variable dissicnat            in table Oyr        was not found in the NEMO output files: task skipped.\|Variable talknat              in table Oyr        was not found in the NEMO output files: task skipped.\|The grid opa_grid_1point consists of a single point which is not supported, dismissing variables masso in Omon,volo in Omon,zostoga in Omon,thetaoga in Omon,bigthetaoga in Omon,tosga in Omon,soga in Omon,sosga in Omon\| zhalfo\| Dimension ncatice could not be found in file.*lim_grid_T_2D.nc, inserting using length-one dimension instead"
#BULL        | grep -v "Variable dissicnat            in table Oyr        was not found in the NEMO output files: task skipped.\|Variable talknat              in table Oyr        was not found in the NEMO output files: task skipped.\|The grid opa_grid_1point consists of a single point which is not supported, dismissing variables masso in Omon,volo in Omon,zostoga in Omon,thetaoga in Omon,bigthetaoga in Omon,tosga in Omon,soga in Omon,sosga in Omon"
    echo
done

cat << EOT

In case of problem, you can start investigate with:

   ./sub-cmor.sh -v -c onep ifs 11

EOT
