#! /usr/bin/env python

DOC='''Scale cmorized PMx variables at non-surface levels; move them to another
directory'''

import os
import shutil
import subprocess
import uuid
import glob, sys
import argparse
import errno
import logging
import numpy as np
from netCDF4 import Dataset

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

if __name__ == "__main__":

    # ONLY exps that need it!
    #
    #                 experiment_id    activity_id
    #         EXP     XPRMNT           MIP
    runs = [ ('onep', '1pctCO2',       'CMIP'       ),
             ('amip', 'amip',          'CMIP'       ),
             ('hpae', 'hist-piAer',    'AerChemMIP' ),
             ('hsp4', 'histSST-piCH4', 'AerChemMIP' ) ]

    parser = argparse.ArgumentParser(description=DOC)
    parser.add_argument('-d', '--dryrun', help='dry-run', action='store_true')
    parser.add_argument('exp', help='4-letter experiment name to process')

    args = parser.parse_args()
    try:
        NNN = [i[0] for i in runs].index(args.exp)
    except ValueError:
        print "Unknown experiment: {}".format(args.exp)
        sys.exit(1)

    EXP, XPRMNT, MIP = runs[NNN]
    ECEMODEL='EC-EARTH-AerChem'

    eff = not args.dryrun

    logging.basicConfig(filename='log/non-cmip6-PMX-{}.log'.format(EXP),
                        format='%(levelname)s:%(message)s', level=logging.INFO)

    # PMx variables
    var = [ ('mmrpm1',   False),
            ('mmrpm10',  False),
            ('mmrpm2p5', False) ]

    for v, skipit in var:

        if skipit:
            logging.info("Skip {}".format(v))
            continue

        #  1st part of path from own script around ece2cmor3
        rootdata="/scratch/ms/nl/nm6/cmorised-results/{}-{}-{}/{}".format(ECEMODEL, MIP, XPRMNT, EXP)

        #  2nd part from CMORization starts with
        srcdir = rootdata + "/CMIP6"
        tgtdir = rootdata + "/NON-CMIP6"

        # Find all files for that variable
        for root,dirs,files in os.walk(os.path.expanduser(srcdir)):
            for f in files:
                if v+'_' in f:
                    src = os.path.join(root,f)

                    newroot = root.replace(srcdir, tgtdir)
                    if eff: mkdir_p(newroot)
                    tgt = os.path.join(newroot,f)
                    src_bak = '{}.bak'.format(tgt)

                    if os.path.isfile(src_bak):
                        raise RuntimeError('Backup file "{}" exists already!'.format(src_bak))
                        #pass
                    else:
                        if eff:
                            shutil.copy2(src=src, dst=src_bak)
                        else:
                            print 'src    : {}'.format(src.replace(rootdata,''))
                            print 'src_bak: {}'.format(src_bak.replace(rootdata,''))
                            print 'tgt    : {}'.format(tgt.replace(rootdata,''))
                            print ''

                    airmass = src.replace(v, 'airmass')
                    if not os.path.isfile(airmass):
                        raise RuntimeError('Airmass file "{}" does NOT exist!'.format(airmass))

                    logging.info("Scale what/with:\n  {}\n  {}".format(src,airmass))

                    dvar = Dataset(src, 'r+')
                    dair = Dataset(airmass, 'r')
                    try:
                        dat = np.array(dvar.variables[v][:])
                        air = np.array(dair.variables['airmass'][:])
                        surfair = (air[:,0,:,:]).reshape(12,1,90,120)
                        data = dat * (surfair/air)
                    except KeyError:
                        raise RuntimeError('Variable "{}" not found in "{}"!'.format(variable_name, file_name))

                    if eff: dvar.variables[v][:] = data
                    dvar.close()
                    dair.close()

                    logging.info("Rename:\n  {}\n  {}".format(src,tgt))
                    if eff: os.rename(src, tgt)

                    ## test one is enough
                    #if args.dryrun:
                    #    sys.exit(0)

