#!/bin/bash
#PBS -q ns
#PBS -l EC_billing_account=nlchekli
#PBS -j oe
#PBS -l walltime=3:30:00

cd $PBS_O_WORKDIR

# ./fix-AERmon-PMx.py hpae

cd /home/ms/nl/nm6/ECEARTH/proj/ece2cmor/

#echo; echo --- pls: year 1865 ---; echo
#./move-duplicates.sh hspr 1865
echo; echo --- pls: year 1881 ---; echo
./move-duplicates.sh hspr 1881
echo; echo --- pls: year 1899 ---; echo
./move-duplicates.sh hspr 1899
echo; echo --- pls: year 1900 ---; echo
./move-duplicates.sh hspr 1900
echo; echo --- pls: year 1919 ---; echo
./move-duplicates.sh hspr 1919
echo; echo --- pls: year 1940 ---; echo
./move-duplicates.sh hspr 1940
echo; echo --- pls: year 1969 ---; echo
./move-duplicates.sh hspr 1969
echo; echo --- pls: year 1974 ---; echo
./move-duplicates.sh hspr 1974
