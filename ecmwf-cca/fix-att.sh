#!/bin/bash
#PBS -q ns
#PBS -l EC_billing_account=spnltune
#PBS -j oe
##PBS -l walltime=0:30:00

module load nco
set -eu

EXP=xh2t

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem
# exception
[[ $EXP = 'xh2t' ]] && ECEMODEL=EC-EARTH3P-HR

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    xh2t) XPRMNT=highres-future ;       MIP=HighResMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        usage
        exit 1
esac

if [[ $PLATFORM == 'RHINO' ]]
then
    datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
elif [[ $PLATFORM == 'CCA' ]]
then
    datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
fi

cd $datadir

root=CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3-AerChem/$XPRMNT/r1i1p1f1
[[ $EXP = 'xh2t' ]] && root=CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3P-HR/$XPRMNT/r2i1p2f1

cd $root

fff=$(find . -type f -name "*.nc")

for f in $fff
do
    ncatted -h -O -a variant_label,global,o,c,"r2i1p2f1" -a realization_index,global,o,l,2 $f
done

echo SUCCESS




