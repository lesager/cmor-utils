#!/bin/bash
#PBS -q ns
#PBS -l EC_billing_account=nlmonkli
#PBS -j oe
##PBS -l walltime=0:30:00

 #######################################################
 # HACKED TO INCLUDE 2050 DATA TO RESPECTIVE ARCHIVES  #
 #######################################################

##############################################################################################
# Script to efficiently pack the cmorized data from the 2050-2100 hires primavera extensions #
##############################################################################################

set -eu

# Either s2hh or xh2t - nothing else supported
EXP=s2hh

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem
# exception
[[ $EXP = 'xh2t' || $EXP = 's2hh' ]] && ECEMODEL=EC-EARTH3P-HR

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;           MIP=CMIP ;;
    pict) XPRMNT=piControl ;            MIP=CMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;         MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;          MIP=AerChemMIP ;;
    s001) XPRMNT=ssp370 ;               MIP=ScenarioMIP ;;
    s002) XPRMNT=ssp370-lowNTCF ;       MIP=AerChemMIP ;;
    s003) XPRMNT=ssp370-lowNTCFCH4 ;    MIP=AerChemMIP ;;
    sst5) XPRMNT=ssp370SST ;            MIP=AerChemMIP ;;
    sst6) XPRMNT=ssp370SST-lowNTCF ;    MIP=AerChemMIP ;;
    sst7) XPRMNT=ssp370SST-lowNTCFCH4 ; MIP=AerChemMIP ;;
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    pic1) XPRMNT=piClim-control ; MIP=RFMIP ;;
    pic2) XPRMNT=piClim-NTCF ;    MIP=AerChemMIP ;;
    pic3) XPRMNT=piClim-CH4 ;     MIP=AerChemMIP ;;
    xh2t) XPRMNT=highres-future ;       MIP=HighResMIP ;;
    s2hh) XPRMNT=highres-future ;       MIP=HighResMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        usage
        exit 1
esac

if [[ $PLATFORM == 'RHINO' ]]
then
    datadir=/lustre3/projects/CMIP6/sager/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
elif [[ $PLATFORM == 'CCA' ]]
then
    datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
fi

cd $datadir

root=CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3-AerChem/$XPRMNT/r1i1p1f1
[[ $EXP = 'xh2t' ]] && root=CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3P-HR/$XPRMNT/r2i1p2f1
[[ $EXP = 's2hh' ]] && root=CMIP6/${MIP}/EC-Earth-Consortium/EC-Earth3P-HR/$XPRMNT/r1i1p2f1

[[ $EXP = 'xh2t' ]] && sfce='ec:/nm6/EC-EARTH/proj/primavera_extension_sybren_r2'
[[ $EXP = 's2hh' ]] && sfce='ec:/nm6/EC-EARTH/proj/primavera_extension_sybren_r1'


# Using a file list for the remaining
flist=/scratch/ms/nl/nm6/cmorised-results/flist

# -- 5 
# 'Omon' that cannot be packed at once (separated in 5 decades)
# X     108G thetao
# X      65G thkcello
# X      82G so
# X     118G umo
# X     118G uo
# X     118G vmo
# X     118G vo
# X     119G wmo

for var in thetao thkcello so umo uo vmo vo wmo
do
#    for k in {5..9}
#    do
k=5
        j=$(( k+1 ))
        i=$(printf %02d $j)
        fflist=$flist-Omon-$var-20${k}0-2${i}0
        [[ -f $fflist ]] && rm -f $fflist
        tgt=EC-Earth3P-HR-$XPRMNT-Omon-$var-20${k}0-2${i}0.tar
        echo ; echo *II* $tgt; echo
    
        #echo $tgt > $flist$k
        find $root/Omon -name ${var}_*20$k[0-9]01-*12.nc >> $fflist
        find $root/Omon -name ${var}_*2${i}001-*12.nc >> $fflist

        tar -cvf $tgt -T $fflist
        emv $tgt $sfce/$tgt
#    done
done

# -- 6
# 'Day' that cannot be packed at once (separated in 10 chunks of 5 years)
# X      212G ta
# X      264G ua
# X      272G wap
# X      207G zg
# X      270G va
# X      254G hur
# X      228G hus

for var in ta ua wap zg va hur hus
do
#    for k in $(seq 51 5 96)
#    do
k=50
        j=$(( k+5 ))
        i=$(printf %03d $j)
        fflist=$flist-day-$var-20${k}-2${i}
        [[ -f $fflist ]] && rm -f $fflist
        tgt=EC-Earth3P-HR-$XPRMNT-day-$var-20${k}-2${i}.tar
        echo ; echo *II* $tgt; echo
        
        for m in $(eval echo {${k}..${j}})
        do
            m3=$(printf %03d $m)
            find $root/day -name ${var}_*gr_2${m3}0101-2${m3}1231.nc >> $fflist
        done
        tar -cvf $tgt -T $fflist
        emv $tgt $sfce/$tgt
#    done
done


# -- 7
# 'Day' that cannot be packed at once (separated in 2 chunks of 25 years)
# X       32G hursmin
# X       32G hfls
# X       33G hfss
# X       32G huss
# X       33G sfcWind
# X       33G sfcWindmax

for var in hursmin hfls hfss huss sfcWind sfcWindmax
do

        fflist=$flist-day-$var-2050-2075
        [[ -f $fflist ]] && rm -f $fflist
        tgt=EC-Earth3P-HR-$XPRMNT-day-$var-2050-2075.tar
        echo ; echo *II* $tgt; echo
        
        for m in {50..75}
        do
            m3=$(printf %03d $m)
            find $root/day -name ${var}_*gr_2${m3}0101-2${m3}1231.nc >> $fflist
        done
        tar -cvf $tgt -T $fflist
        emv $tgt $sfce/$tgt

done
