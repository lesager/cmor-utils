#! /usr/bin/env python

DOC=""" Take a sftlf_fx_EC-Earth3*.nc file created by ece2cmor3 (v1.7 and above?),
and replace the data with their correct values generated from oasis mask.
"""
# /scratch/ms/nl/nm6/cmorised-results-BIS/EC-EARTH-HR-CMIP-historical/hires-1y/CMIP6/HighResMIP/EC-Earth-Consortium/EC-Earth3P/highres-future/r2i1p2f1/Ofx/sftof/gn/v20210415/sftof_Ofx_EC-Earth3P_highres-future_r2i1p2f1_gn.nc
import os
import shutil
import uuid
import glob, sys
import argparse

from netCDF4 import Dataset

# parser = argparse.ArgumentParser(description=DOC)
# parser.add_argument('exp', help='4-letter experiment name to consider')

# args = parser.parse_args()

# -- Template with correct data, but wrong global metadata
#
#-- creation -- cd /perm/ms/nl/nm6/DATA-PRIMAVERA-STR2-XTRA/oasis/T511-ORCA025/
#-- creation -- cdo -f grb selvar,A256.msk masks.nc sftlf.gb
#-- creation --
#-- creation -- cd ifs/T511L91/19500101/
#-- creation -- cdo griddes ICMGGECE3INIT > red511
#-- creation --
#-- creation -- cdo setgrid,red511 sftlf.gb sftlf-rg.gb
#-- creation -- cdo -R -f nc4c -z zip chname,var1,sftlf -mulc,100. sftlf-rg.gb sftlf.nc

## Choose one template
#HIRES   template_file = "/perm/ms/nl/nm6/DATA-PRIMAVERA-STR2-XTRA/oasis/T511-ORCA025/sftlf.nc"
#STD-RES template_file = '../ece2cmor3/ece2cmor3/scripts/data-qa/recipes/sftlf_fx_EC-Earth3-Veg_TEMPLATE_r1i1p1f1_gr.nc'

template_file = '../ece2cmor3/ece2cmor3/scripts/data-qa/recipes/sftlf_fx_EC-Earth3-Veg_TEMPLATE_r1i1p1f1_gr.nc'

# -- Target (existing, has good metadata because produced by ece2cmor3, but wrong data)

#file_name='/perm/ms/nl/nm6/primavera-fx-Ofx-EC-Earth3P-HR/fx/sftlf/gr/v20210412/sftlf_fx_EC-Earth3P-HR_highres-future_r2i1p2f1_gr.nc'
file_name = '/scratch/ms/nl/nm6/cmorised-results-BIS/EC-EARTH-HR-CMIP-historical/hi1y/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-AerChem/piControl/r1i1p1f1/fx/sftlf/gr/v20210416/sftlf_fx_EC-Earth3-AerChem_piControl_r1i1p1f1_gr.nc'
file_name_bak = '{}.bak'.format(file_name)

print(file_name)
print(file_name_bak)

if os.path.isfile(file_name_bak):
    raise RuntimeError('Backup file "{}" exists already!'.format(file_name_bak))

shutil.copy2(src=file_name, dst=file_name_bak)

# -- Replace

ds = Dataset(file_name, 'r+')
ref = Dataset(template_file,'r')

data = ref.variables['sftlf'][:]

# flip data from hires template
#ds.variables['sftlf'][:] = data[::-1,...]
ds.variables['sftlf'][:] = data

ref.close()
ds.close()
