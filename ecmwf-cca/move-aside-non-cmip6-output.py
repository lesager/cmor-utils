#!/usr/bin/env python

import sys
import os
import errno
import argparse
import logging

ERRA=''' Move cmorized variables not known to CMIP6 to another
directory '''

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=ERRA)
    parser.add_argument('exp', help='4-letter experiment name to consider')

    #         EXP     XPRMNT                  MIP           
    runs = [ ('hist', 'historical',           'CMIP'       ),
             ('pict', 'piControl',            'CMIP'       ),
             ('a4co', 'abrupt-4xCO2',         'CMIP'       ),
             ('piNF', 'hist-piNTCF',          'AerChemMIP' ),
             ('s001', 'ssp370',               'ScenarioMIP'),
             ('s002', 'ssp370-lowNTCF',       'AerChemMIP' ),
             ('s003', 'ssp370-lowNTCFCH4',    'AerChemMIP' ),
             ('sst5', 'ssp370SST',            'AerChemMIP' ),
             ('sst6', 'ssp370SST-lowNTCF',    'AerChemMIP' ),
             ('sst7', 'ssp370SST-lowNTCFCH4', 'AerChemMIP' ),
             ('onep', '1pctCO2',       'CMIP'       ),
             ('hpae', 'hist-piAer',    'AerChemMIP' ),
             ('amip', 'amip',          'CMIP'       ),
             ('s004', 'ssp370pdSST',   'AerChemMIP' ),
             ('hsp4', 'histSST-piCH4', 'AerChemMIP' ),
             ('hspr', 'histSST-piAer', 'AerChemMIP' ),
             ('pic1', 'piClim-control', 'RFMIP'),
             ('pic2', 'piClim-NTCF',    'AerChemMIP'),
             ('pic3', 'piClim-CH4',     'AerChemMIP'),
             ('pic4', 'piClim-aer',     'RFMIP')]

    args = parser.parse_args()
    try:
        NNN = [i[0] for i in runs].index(args.exp)
    except ValueError:
        print "Unknown experiment: {}".format(args.exp)
        sys.exit(1)

    EXP, XPRMNT, MIP = runs[NNN]
    ECEMODEL='EC-EARTH-AerChem'

    logging.basicConfig(filename='log/non-cmip6-{}.log'.format(EXP),
                        format='%(levelname)s:%(message)s', level=logging.INFO)
    
    # Those marked True are known to CMIP6
    var = [ ('rsutaf',   True),
            ('rsutcsaf', True),
            ('rlutcsaf', True),
            ('rlutaf',   True),
            ('mmrpm1',   False), # Known to CMIP6 but differently from what we get
            ('mmrpm10',  False), # Known to CMIP6 but differently from what we get
            ('mmrpm2p5', False), # Known to CMIP6 but differently from what we get
            ('reffclws', False), # Known to CMIP6 but differently from what we get
            ('rsscsaf',  False),
            ('rssaf',    False),
            ('rlscsaf',  False),
            ('rlsaf',    False),
            ('rsdscs',   False),  # Buggy see issue #826 on EC-Earth portal
            ('rsuscs',   False)]  # Buggy see issue #826 on EC-Earth portal

    # Not efficient loop order, but good enough for the job at hand
    for v,iscmip6 in var:

        if iscmip6:
            logging.info("Skip {}".format(v))
            continue
        else:
            logging.info("Process {}".format(v))
        
        #   1st part of path from own script around ece2cmor3
        rootdata="/scratch/ms/nl/nm6/cmorised-results/{}-{}-{}/{}".format(ECEMODEL, MIP, XPRMNT, EXP)
        print(rootdata)
        #   2nd part from CMORization starts with
        srcdir = rootdata + "/CMIP6"
        tgtdir = rootdata + "/NON-CMIP6"
        
        # find all files for that variable
        for root,dirs,files in os.walk(os.path.expanduser(srcdir)):
            for f in files:
                if v+'_' in f:
                    src = os.path.join(root,f)
                    newroot = root.replace(srcdir, tgtdir)
                    mkdir_p(newroot)
                    tgt = os.path.join(newroot,f)
                    logging.info("Rename:\n  {}\n  {}".format(src,tgt))
                    os.rename(src, tgt)
                
        
