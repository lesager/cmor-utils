#!/bin/bash

#PBS -N cf-hspr
#PBS -q nf
#PBS -l EC_billing_account=nlmonkli
#PBS -j oe
#PBS -l EC_hyperthreads=1
#PBS -l EC_total_tasks=1
#PBS -l EC_threads_per_task=18
#PBS -o log/fixer-final-dry-run-check/cf-pic4-check

EXP=pic4

set -e
error() { echo "ERROR: $1" >&2; exit 1; }

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem
PEXTRA='-pextra'

# Specific Experiment Def
case $EXP in
    hist) XPRMNT=historical ;    MIP=CMIP ;;
    pict) XPRMNT=piControl ;     MIP=CMIP ;;
    piNF) XPRMNT=hist-piNTCF ;   MIP=AerChemMIP ;;
    a4co) XPRMNT=abrupt-4xCO2 ;  MIP=CMIP ;;
    onep) XPRMNT=1pctCO2 ;       MIP=CMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    hspr) XPRMNT=histSST-piAer ; MIP=AerChemMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    pic4) XPRMNT=piClim-aer ;    MIP=RFMIP ;;
    *) echo "wrong exp: $EXP"
        exit 1
esac

# Experiment control output setup
if [[ $MIP = 'CMIP' ]]
then
    CTRLDIR=/scratch/ms/nl/nm6/SUBDIRS/${XPRMNT}/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/${ECEMODEL}/cmip6-experiment-${MIP}-${XPRMNT}
else
    CTRLDIR=/scratch/ms/nl/nm6/SUBDIRS/${XPRMNT}/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/cmip6-experiment-${MIP}-${XPRMNT}
fi

# Hacks for 'onep' and 'xh2t|s2hh' (primavera), and 'hspr' experiment cases
[[ $EXP = 'hspr' || $EXP = 'pic4' ]] && \
    CTRLDIR=/scratch/ms/nl/nm6/3.3-AerChem/runtime/classic/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/cmip6-experiment-${MIP}-${XPRMNT}
[[ $EXP = 'onep' ]] && \
    CTRLDIR=/scratch/ms/nl/nm6/3.3.2.1-aerchemmip-RT/classic/ctrl/cmip6-output-control-files${PEXTRA}/${MIP}/${ECEMODEL}/cmip6-experiment-${MIP}-${XPRMNT}
[[ $EXP = 'xh2t' || $EXP = 's2hh' ]] && \
    CTRLDIR=/perm/ms/nl/nm6/primavera-stream2-ctrl

METADATA=${CTRLDIR}/metadata-cmip6-${MIP}-${XPRMNT}-${ECEMODEL}-$COMPONENT-template.json

# CMOR output
ODIR=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
[[ ! -d $ODIR ]] && error "Directory $ODIR does not exist"
[[ -z $(ls -A $ODIR) ]] && error "Empty dir: $ODIR"

. /scratch/ms/nl/nm6/miniconda3/etc/profile.d/conda.sh
conda activate cmorfixer

export HDF5_USE_FILE_LOCKING=FALSE

cd /perm/ms/nl/nm6/cmorize/cmor-fixer
./cmor-fixer.py --dry --verbose --keepid --olist --npp 18 $ODIR

