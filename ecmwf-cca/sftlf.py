#! /usr/bin/env python

DOC=""" Take a template file (sftlf_fx_EC-Earth3-Veg_TEMPLATE_r1i1p1f1_gr.nc),
which has correct data values for fx/sftlf (land fraction) and create
publishable files for selected experiment.
"""

import os
import shutil
import uuid
import glob, sys
import argparse

from netCDF4 import Dataset

#                 experiment_id   activity_id
#         EXP     XPRMNT          MIP            version (DUMMY)
runs = [ ('hist', 'historical',   'CMIP',        'v20200121' ),
         ('pict', 'piControl',    'CMIP',        'v20200121' ),
         ('a4co', 'abrupt-4xCO2', 'CMIP',        'v20200121' ),
         ('piNF', 'hist-piNTCF',  'AerChemMIP',  'v20200121' ),
         ('onep', '1pctCO2',      'CMIP',        'v20200121' ),
         ('amip', 'amip',         'CMIP',        'v20200121' ),
         ('s001', 'ssp370',       'ScenarioMIP', 'v20200121' ),
         ('s002', 'ssp370-lowNTCF', 'AerChemMIP', 'v20200121' ),
         ('s003', 'ssp370-lowNTCFCH4', 'AerChemMIP', 'v20200121' ),
         ('s004', 'ssp370pdSST',       'AerChemMIP', 'v20200121' ),
         ('sst5', 'ssp370SST',         'AerChemMIP', 'v20200121' ),
         ('sst6', 'ssp370SST-lowNTCF', 'AerChemMIP', 'v20200121' ),
         ('sst7', 'ssp370SST-lowNTCFCH4', 'AerChemMIP', 'v20200121' ),
         ('hpae', 'hist-piAer',    'AerChemMIP', 'v20200121' ),
         ('hsp4', 'histSST-piCH4', 'AerChemMIP', 'v20200121' ) ]

parser = argparse.ArgumentParser(description=DOC)
parser.add_argument('exp', help='4-letter experiment name to consider')

args = parser.parse_args()
try:
    NNN = [i[0] for i in runs].index(args.exp)
except ValueError:
    print "Unknown experiment: {}".format(args.exp)
    sys.exit(1)

EXP, XPRMNT, MIP, version = runs[NNN]
ECEMODEL='EC-EARTH-AerChem'

# -- Template with correct data
template_file = "ece2cmor3/ece2cmor3/scripts/data-qa/recipes/sftlf_fx_EC-Earth3-Veg_TEMPLATE_r1i1p1f1_gr.nc"


# -- Find tas from the same experiment - this is REFERENCE for metadata

#   1st part of path from own script around ece2cmor3
rootdata="/scratch/ms/nl/nm6/cmorised-results/{}-{}-{}/{}".format(ECEMODEL, MIP, XPRMNT, EXP)

#   2nd part from CMORization
inidata = rootdata + "/CMIP6/{}/EC-Earth-Consortium/EC-Earth3-AerChem/{}/r1i1p1f1".format(MIP, XPRMNT)

tasfiles = inidata + "/Amon/tas/gr/v2020*/tas_Amon_EC-Earth3-AerChem_{}_r1i1p1f1_gr_185001-185012.nc".format(XPRMNT)
if EXP.startswith('amip'):
 tasfiles = inidata + "/Amon/tas/gr/v2020*/tas_Amon_EC-Earth3-AerChem_{}_r1i1p1f1_gr_198001-198012.nc".format(XPRMNT)
if EXP.startswith('s'):
 tasfiles = inidata + "/Amon/tas/gr/v2020*/tas_Amon_EC-Earth3-AerChem_{}_r1i1p1f1_gr_201501-201512.nc".format(XPRMNT)
tas = glob.glob(tasfiles)

N = len(tas)

if N != 1:
    print "We have a problem: found {} TAS file(s) in \n\t {}".format(N, tasfiles)
    if N > 1:
        print '\n\t'.join(tas)
    sys.exit(1)
else:
    print " Reference file for global attributes:"
    print "       {}".format(tas[0])


# -- Create target

dir_name = rootdata + '/CMIP6/' + MIP + '/EC-Earth-Consortium/EC-Earth3-AerChem/' + XPRMNT + '/r1i1p1f1/fx/sftlf/gr/' + version
file_name = os.path.join(dir_name, 'sftlf_fx_EC-Earth3-AerChem_' + XPRMNT + '_r1i1p1f1_gr.nc')

print " Target file:"
print "       {}".format(file_name)

if not os.path.exists(dir_name):
    os.makedirs(dir_name)

shutil.copy2(src=template_file, dst=file_name)

ds = Dataset(file_name, 'r+')


# -- Replace all attributes with those from the reference file, except a few strictly related to the SFTLF variable

do_not_touch = ['frequency','table_id','table_info','tracking_id','variable_id']

ref = Dataset(tas[0],'r')

for name in ref.ncattrs():
    if not name in do_not_touch:
        ds.setncattr(name, getattr(ref, name))
        print "Att. {} overwritten".format(name)
    else:
        print "DID NOT OVERWRITE {}".format(name)

ref.close()

ds.tracking_id = 'hdl:21.14100/' + str(uuid.uuid4())

ds.close()
