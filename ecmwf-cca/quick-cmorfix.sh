#!/bin/bash

#PBS -N qcmf
#PBS -q nf
#PBS -l EC_billing_account=nlmonkli
#PBS -j oe
#PBS -l EC_hyperthreads=1
#PBS -l EC_total_tasks=1
#PBS -l EC_threads_per_task=18
#PBS -l walltime=00:30:00

set -e

error() {
    echo "ERROR: $1" >&2
    exit 1
}

# CMOR output
ODIR='/scratch/ms/nl/nm6/cmorised-results-BIS/EC-EARTH-HR-CMIP-historical/hi1y/CMIP6'
[[ ! -d $ODIR ]] && error "Directory $ODIR does not exist"
[[ -z $(ls -A $ODIR) ]] && error "Empty dir: $ODIR"

. /scratch/ms/nl/nm6/miniconda3/etc/profile.d/conda.sh
conda activate cmorfixer

export HDF5_USE_FILE_LOCKING=FALSE

cd /perm/ms/nl/nm6/cmorize/cmor-fixer
./cmor-fixer.py  --verbose --keepid --olist --npp 18 $ODIR

