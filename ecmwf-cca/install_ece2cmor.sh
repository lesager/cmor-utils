#!/usr/bin/env bash

#PBS -N instaconda.jb
#PBS -q ns
#PBS -l EC_billing_account=spnltune
#PBS -j oe
#PBS -o instamini.out

set -e

# --- conda ----

#condaversion=Anaconda2-2019.03-Linux-x86_64.sh
condaversion=Anaconda2-5.3.0-Linux-x86_64.sh

#miniversion2=Miniconda2-4.5.12-Linux-x86_64.sh
miniversion2=Miniconda2-latest-Linux-x86_64.sh

#miniversion3=
miniversion3=Miniconda3-latest-Linux-x86_64.sh

mini2=0
mini3=1
conda=0

getit=1
clean=0

export http_proxy=http://proxy:2222
export https_proxy=http://proxy:2222
module load git

cd $SCRATCH

# --- Condas ----
if (( mini2 ))
then
    echo
    echo "*II* Install Miniconda2"
    (( getit )) && wget https://repo.anaconda.com/miniconda/${miniversion2}
    chmod u+x ${miniversion2}
    (( clean )) && rm -rf $SCRATCH/miniconda2
    ./${miniversion2} -b -p $SCRATCH/miniconda2
    exit
fi

if (( mini3 ))
then
    echo
    echo "*II* Install Miniconda3"
    (( getit )) && wget https://repo.anaconda.com/miniconda/${miniversion3}
    chmod u+x ${miniversion3}
    (( clean )) && rm -rf $SCRATCH/miniconda3
    ./${miniversion3} -b -u -p $SCRATCH/miniconda3
    exit
fi

if (( conda ))
then
    (( getit )) && wget http://repo.continuum.io/archive/${condaversion}
    chmod u+x ${condaversion}
    (( clean )) && rm -rf $SCRATCH/anaconda2
    ./${condaversion} -b -p $SCRATCH/anaconda2
    exit
fi

# --- cmor-fixer ----
cd $PERM/cmorize

echo
echo "*II* Install cmor-fixer"
git clone https://github.com/EC-Earth/cmor-fixer.git






# --- ece2cmor3 ----
cd $PERM/cmorize
echo
echo "*II* Install ece2cmor3"
#rm -rf ece2cmor3
#mkdir -p ece2cmor3
git clone https://github.com/EC-Earth/ece2cmor3.git
cd ece2cmor3
git submodule update --init --recursive


#########
#########  CommandNotFoundError: Your shell has not been properly configured to use 'conda activate'.
#########  If your shell is Bash or a Bourne variant, enable conda for the current user with
#########   
#########      $ echo ". /scratch/ms/nl/nm6/miniconda2/etc/profile.d/conda.sh" >> ~/.bashrc
#########   
#########  or, for all users, enable conda with
#########   
#########      $ sudo ln -s /scratch/ms/nl/nm6/miniconda2/etc/profile.d/conda.sh /etc/profile.d/conda.sh
#########   
#########  The options above will permanently enable the 'conda' command, but they do NOT
#########  put conda's base (root) environment on PATH.  To do so, run
#########   
#########      $ conda activate
#########   
#########  in your terminal, or to put the base environment on PATH permanently, run
#########   
#########      $ echo "conda activate" >> ~/.bashrc
#########   
#########  Previous to conda 4.4, the recommended way to activate conda was to modify PATH in
#########  your ~/.bashrc file.  You should manually remove the line that looks like
#########   
#########      export PATH="/scratch/ms/nl/nm6/miniconda2/bin:$PATH"
#########   
#########  ^^^ The above line should NO LONGER be in your ~/.bashrc file! ^^^
#########

. /scratch/ms/nl/nm6/miniconda2/etc/profile.d/conda.sh

# --- conda env ----
echo
echo "*II* Create ece2cmor3 conda env"
cd ${PERM}/cmorize/ece2cmor3

# conda env remove --name ece2cmor3
# conda clean --all -y                   # recommended
# conda update -n base -c defaults conda # for updating conda itself

conda env create -f environment.yml
conda activate ece2cmor3
python setup.py install
conda deactivate

# --- test install ----
echo
echo "*II* Test"
conda activate ece2cmor3

cd ${PERM}/cmorize/ece2cmor3

ece2cmor -h
ece2cmor --version
drq -h
drq -v
cd ${PERM}/cmorize/ece2cmor3/ece2cmor3/
./scripts/checkvars.py -h
conda deactivate
