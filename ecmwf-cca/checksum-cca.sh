#!/usr/bin/sh

(( $# != 1 )) && echo NEED EXP ARGUMENT && exit 1

EXP=$1

# Switch
NON="NON-"
#NON=

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    hspr) XPRMNT=histSST-piAer ; MIP=AerChemMIP ;;
    pic4) XPRMNT=piClim-aer ;    MIP=RFMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
file_list="file.list."$NON$EXP
checksum_file="sha256sums-"$NON$EXP

qsub <<EOF
#!/bin/bash
#PBS -N csum-$NON$EXP
#PBS -q nf
#PBS -l EC_billing_account=nlmonkli
#PBS -j oe
#PBS -l EC_hyperthreads=1
#PBS -l EC_total_tasks=18
#PBS -o log/checksum-$NON$EXP.log

#export OMP_NUM_THREADS=$EC_threads_per_task

# Uses GNU parallel for parallesisation of checksum computation. Make sure it's
# available! (on NSC, load module parallel/...)

cd $datadir

find ${NON}CMIP6 -type f > $file_list

parallel -a $file_list sha256sum > $checksum_file

EOF
