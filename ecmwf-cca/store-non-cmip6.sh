#!/usr/bin/sh

(( $# != 1 )) && echo NEED EXP ARGUMENT && exit 1

EXP=$1

# Switch
NON="NON-"

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    hspr) XPRMNT=histSST-piAer ; MIP=AerChemMIP ;;
    pic4) XPRMNT=piClim-aer ;    MIP=RFMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        exit 1
esac

datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
file_list="file.list."$NON$EXP
checksum_file="sha256sums-"$NON$EXP

ecfs_dir="ec:/nm6/EC-EARTH/proj/EC-Earth-AerChem-CMIP6"
TGT=NONCMIP6-AerChem-${XPRMNT}.tar

qsub <<EOF
#!/bin/bash
#PBS -N store-$NON$EXP
#PBS -q ns
#PBS -l EC_billing_account=nlmonkli
#PBS -j oe
#PBS -l EC_ecfs=1
#PBS -o log/store-$EXP.log

set -e

bckp_emcp () {
    emv -e \$1  ${ecfs_dir}/\$1
    echmod 444 ${ecfs_dir}/\$1
}

maxsize=34359738368             # limit in bytes for emv as of October 2017 (32GB)

split_move () {
    # split if larger than what emv can handle (34359738368 bytes)
    local f=\$1
    actualsize=\$(du -b "\$f" | cut -f 1)
    if (( \$actualsize > \$maxsize )); then
        nn=\$(( \$actualsize / \$maxsize + 1))
        split -n \$nn -a 1 -d \$f \${f}_
        \rm -f \$f
        for k in \$(eval echo {0..\$((nn-1))})
        do 
            bckp_emcp \${f}_\${k}
        done
    else
        bckp_emcp \$f
    fi
}

not_empty_dir () {
    [[ ! -d "\$1" ]] && return 1
    [ -n "\$(ls -A \$1)" ] && return 0 || return 1
}

cd $datadir
tar -cvf $TGT NON-CMIP6 $file_list $checksum_file
split_move $TGT

EOF
