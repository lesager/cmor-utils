#! /usr/bin/env python

DOC=""" Wrapper around sftof.py from ece2cmor3 to find file to process easily"""

import os
import shutil
import subprocess
import uuid
import glob, sys
import argparse

from netCDF4 import Dataset

#                 experiment_id   activity_id
#         EXP     XPRMNT          MIP            version
runs = [ ('onep', '1pctCO2',      'CMIP',        'v20200121' ),
         ('amip', 'amip',         'CMIP',        'v20200121' ),
         ('s004', 'ssp370pdSST',  'AerChemMIP',  'v20200121' ),
         ('hpae', 'hist-piAer',    'AerChemMIP', 'v20200121' ),
         ('hsp4', 'histSST-piCH4', 'AerChemMIP', 'v20200121' ) ]

parser = argparse.ArgumentParser(description=DOC)
parser.add_argument('exp', help='4-letter experiment name to consider')

args = parser.parse_args()
try:
    NNN = [i[0] for i in runs].index(args.exp)
except ValueError:
    print "Unknown experiment: {}".format(args.exp)
    sys.exit(1)

EXP, XPRMNT, MIP, version = runs[NNN]
ECEMODEL='EC-EARTH-AerChem'

# -- Script
scrpt="ece2cmor3/ece2cmor3/scripts/data-qa/scripts/sftof.py"


# -- Find SFTOF file to fix

#   1st part of path from own script around ece2cmor3
rootdata="/scratch/ms/nl/nm6/cmorised-results/{}-{}-{}/{}".format(ECEMODEL, MIP, XPRMNT, EXP)

#   2nd part from CMORization
inidata = rootdata + "/CMIP6/{}/EC-Earth-Consortium/EC-Earth3-AerChem/{}/r1i1p1f1".format(MIP, XPRMNT)

gfiles = inidata + "/Ofx/sftof/gn/v2020*/sftof_Ofx_EC-Earth3-AerChem_{}_r1i1p1f1_gn.nc".format(XPRMNT)
zefile = glob.glob(gfiles)

N = len(zefile)

if N != 1:
    print "We have a problem: found {} SFTOF file(s)".format(N)
    if N > 1:
        print '\n\t'.join(zefile)
    sys.exit(1)
else:
    print " File to be modified:"
    print "       {}".format(zefile[0])


# -- Apply script

cmd = [ scrpt, zefile[0]]

p = subprocess.Popen( cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout_str, stderr_str = p.communicate()

if p.returncode:
    print(stdout_str, stderr_str)
    raise RuntimeError('\n\terror with command: {}'.format(cmd))

