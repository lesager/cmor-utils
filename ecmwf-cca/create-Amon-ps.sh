#!/bin/bash

set -eu

version=v20200904

usage() {
    cat << EOT >&2

Create missing Amon/ps data from CFday/ps or 3hr/ps data set

Usage: $(basename $0) [-v version] EXPERIMENT YSTART YEND

Options
   -v vYYYYMMDD : specify the version, default $version

EOT
}

# -- options

while getopts "h?v:" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        v) version=$OPTARG
    esac
done
shift $((OPTIND-1))

# -- Arg
if [ "$#" -ne 3 ]; then
    echo; echo " NEED THREE ARGUMENTS, NO MORE, NO LESS!"; echo
    usage
    exit 1
fi
 
EXP=$1
FIRST_YEAR=$2
LAST_YEAR=$3

# Common Experiment Def
ECEMODEL=EC-EARTH-AerChem

# Specific Experiment Def
case $EXP in
    onep) XPRMNT=1pctCO2;        MIP=CMIP ;;
    hpae) XPRMNT=hist-piAer ;    MIP=AerChemMIP ;;
    amip) XPRMNT=amip ;          MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;   MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ; MIP=AerChemMIP ;;
    *)
        echo "Wrong experiment name: $EXP"
        usage
        exit 1
esac

datadir=/scratch/ms/nl/nm6/cmorised-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP/CMIP6
root=$datadir/${MIP}/EC-Earth-Consortium/EC-Earth3-AerChem/$XPRMNT/r1i1p1f1

# create top dir

topdir=$root/Amon/ps/gr/${version}
mkdir -p $topdir

qsub <<EOF
#!/bin/bash
#PBS -N AmonPS-$EXP
#PBS -q nf
#PBS -l EC_billing_account=spnltune
#PBS -j oe
#PBS -l EC_hyperthreads=1
#PBS -l EC_total_tasks=18
#PBS -o log/Amon-ps-$EXP.log

module load cdo
module load nco

# workhorse
doit() {
    local YYYY=\$1
    tgt=$topdir/ps_Amon_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_\${YYYY}01-\${YYYY}12.nc

    src=$root/CFday/ps/gr/${version}/ps_CFday_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_\${YYYY}0101-\${YYYY}1231.nc
    echo Try \$src

    is3=0
    if [[ ! -f \$src ]]
    then
        src=$root/3hr/ps/gr/${version}/ps_3hr_EC-Earth3-AerChem_${XPRMNT}_r1i1p1f1_gr_\${YYYY}01010000-\${YYYY}12312100.nc
        echo Try \$src
        is3=1
    fi

    [[ ! -f \$src ]] && echo "Did not find PS file for \$YYYY" && return 1
    
    echo cdo monmean \$src \$tgt
    #cdo monmean \$src \$tgt

    if (( is3 ))
    then
        ## Need some cleanup if using 3hr (and you must still use the --write flag when calling nctxck from nctime)
        ## Tested/required with cdo 1.8.1
        #\rm -f  \$tgt.bak
        cdo --no_history monmean \$src \$tgt.bak

        cdo -O --no_history settbounds,'month' \$tgt.bak \$tgt
        ncatted -Oh -a cell_methods,ps,o,c,"area: time: mean" \$tgt
        \rm -f  \$tgt.bak
        echo Fix cdo-monmean-3
    else
        cdo --no_history monmean \$src \$tgt
    fi

    ncatted -Oh -a table_id,global,o,c,"Amon" \$tgt
    ncatted -Oh -a tracking_id,global,o,c,"hdl:21.14100/$(uuidgen)" \$tgt
}

t1=\$(date +%s)

export -f doit
seq $FIRST_YEAR $LAST_YEAR | parallel doit

ls -1 $topdir

t2=\$(date +%s)
tr=\$(date -d "0 -\$t1 sec + \$t2 sec" +%T)
echo \$tr

EOF
