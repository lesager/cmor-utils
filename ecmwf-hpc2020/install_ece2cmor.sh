#!/usr/bin/env bash

set -e

cmordir=$PERM/ecearth3/cmorize

reinstall_mamba=false           # DANGEROUS IF YOU HAVE OTHER-THAN-ECE2CMOR3 ENVS !!
reinstall_ece2cmor3=false
reinstall_ece2cmor3_env=false

info() { echo; echo "INFO $@"; echo ; }

# --- Mamba ----
cd $HPCPERM

$reinstall_mamba && rm -rf mamba

if [[ ! -d mamba ]]
then
    info "install mamba"
    # See https://github.com/conda-forge/miniforge#mambaforge
    curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh"
    bash Mambaforge-$(uname)-$(uname -m).sh -b -u -p mamba   # create mamba dir, install therein, -u => do not modify ~/.bashrc I guess
else
    info "mamba already installed"
fi

# Activate
source mamba/etc/profile.d/conda.sh

# Update mamba (note: that will not update your envs)
info "Update mamba"
mamba update -y --name base mamba


# --- install ece2cmor3 and its conda env 

mkdir -p $cmordir; cd $cmordir
$reinstall_ece2cmor3 && rm -rf ece2cmor3

if [[ ! -d ece2cmor3 ]]
then
    info "Retrieve ece2cmor3"
    git clone git@github.com:EC-Earth/ece2cmor3.git

    cd ece2cmor3
    git submodule update --init --recursive
    ./download-b2share-dataset.sh ece2cmor3/resources/b2share-data
else
    info "ece2cmor3 already checked out"
    cd ece2cmor3
fi

if [[ -d $HPCPERM/mamba/envs/ece2cmor3 ]]
then
    info "Remove ece2cmor3 env"
    $reinstall_ece2cmor3_env && conda remove -y -n ece2cmor3 --all
fi

if [[ ! -d $HPCPERM/mamba/envs/ece2cmor3 ]]
then
    info "Create ece2cmor3 env"
    # Install
    conda env create -f environment.yml
    conda activate ece2cmor3
    pip install .
    
    # Add extra variables for AerChemMIP experiments
    cd ece2cmor3/scripts
    ./add-non-cmor-variables.sh

    conda deactivate
else
    info "ece2cmor3 env already created"
fi

# --- test install ----
info "Test installation"
conda activate ece2cmor3

cd $cmordir/ece2cmor3

ece2cmor -h
ece2cmor --version
drq -h
drq -v
cd $cmordir/ece2cmor3/ece2cmor3/
./scripts/checkvars.py -h
conda deactivate

## # --- cmor-fixer ---- NOT NEEDED ANYMORE WITH ECE2CMOR3 1.8...UNTIL A NEW SET OF FIXES IS NEEDED
## cd $cmordir
## 
## echo
## echo "*II* Install cmor-fixer"
## git clone https://github.com/EC-Earth/cmor-fixer.git

# --- ESMValTool

# # install
# mamba create --name esmvaltool esmvaltool
# # check
# conda activate esmvaltool
# esmvaltool --help

#once only:
#esmvaltool config get-config-user ; then edit ${HOME}/.esmvaltool/config-user.yml


#########
#########  CommandNotFoundError: Your shell has not been properly configured to use 'conda activate'.
#########  If your shell is Bash or a Bourne variant, enable conda for the current user with
#########   
#########      $ echo ". /scratch/ms/nl/nm6/miniconda2/etc/profile.d/conda.sh" >> ~/.bashrc
#########   
#########  or, for all users, enable conda with
#########   
#########      $ sudo ln -s /scratch/ms/nl/nm6/miniconda2/etc/profile.d/conda.sh /etc/profile.d/conda.sh
#########   
#########  The options above will permanently enable the 'conda' command, but they do NOT
#########  put conda's base (root) environment on PATH.  To do so, run
#########   
#########      $ conda activate
#########   
#########  in your terminal, or to put the base environment on PATH permanently, run
#########   
#########      $ echo "conda activate" >> ~/.bashrc
#########   
#########  Previous to conda 4.4, the recommended way to activate conda was to modify PATH in
#########  your ~/.bashrc file.  You should manually remove the line that looks like
#########   
#########      export PATH="/scratch/ms/nl/nm6/miniconda2/bin:$PATH"
#########   
#########  ^^^ The above line should NO LONGER be in your ~/.bashrc file! ^^^
#########

