#!/usr/bin/env bash

# Convenient wrapper around "sub-cmor.sh" for setting consistent
# names/logs and checking those once jobs have run.

usage() {
    cat << EOT >&2
Usage: ./${0##*/} [-a account] [-c] [-l] [-v] [-d jobid] <EXP> <COMPONENT> <LEG> [OCTRLDIR] [ECEMODEL]

Submit (default) or check logs of an EC-Earth3 CMORIZATION job
     for ONE experiment, ONE leg, and ONE model component

Options are:
  -a account  : overwrite your default account for computing ressources
  -c          : check logs of a previously submitted job
  -l          : page the log files of previously submitted script with a '${PAGER:-less} <log>' command
  -v          : verbose, i.e. print the name of the files being checked (affects -c only)
  -d jobid    : submit but start only after successful completion of jobid

Submit several jobs (one exp, one model, several legs):

   exp=abcd; model=ifs ; for i in {1..10}; do ./${0##*/} \${exp} \${model} \$i octldir; done

Script echoes jobid (and only that) when successfully submitting a batch job, so chaining jobs is easy:

   rrrr=\$(${0##*/} abcd ifs 11 octldir)
   for ((x = 15 ; x <= 66 ; x += 4)); do rrrr=\$(./${0##*/} -d \${rrrr##* } abcd ifs \${x} octldir); done

Checking older jobs:

   # leg 66 of ifs of experiment abcd
   ./${0##*/} -c abcd ifs 66

EOT
}

set -eu
error() { echo "ERROR: $1" >&2; exit 1 ; }
urror() { echo "ERROR: $1" >&2;  usage; exit 1 ; }

# -- OPTIONS
account= ; verbose=0; chck=0; rundep=; page=0

while getopts "hvca:ld:" opt; do
    case "$opt" in
        h) usage; exit 0 ;;
        a) account=$OPTARG ;;
        c) chck=1 ;;
        d) rundep=$OPTARG ;;
        v) verbose=1 ;;
        l) page=1 ;;
        ?) echo "UNKNOWN OPTION"; usage; exit 1
    esac
done
shift $((OPTIND-1))


# -- ARGS
[[ "$#" -lt 3 ]] && urror "Need AT LEAST THREE ARGUMENTS!"
[[ ! $1 =~ ^[a-Z_0-9]{4}$ ]] && urror "argument EXPERIMENT name (=$1) should be a 4-character string"
[[ ! $3 =~ ^[0-9]+$ ]] && urror "argument LEG_NUMBER (=$3) should be a number"

EXP=$1
COMPONENT=$2
ILEG=$((10#$3))
LEG=$(printf "%03d" $ILEG)

ocd=
if [ "$#" -ge 4 ]; then
    ocd=$4
    [[ ! -d $ocd ]] && error "Directory $ocd does not exist"
    [[ -z $(ls -A $ocd) ]] && error "Empty dir: $ocd"
fi
[[ "$#" -eq 5 ]] && ECEMODEL=$5

tag=${EXP}-${COMPONENT}-$LEG


# -- LOGS
out=$PWD/log/$EXP
mkdir -p $out
log=$out/cmor_$tag.out      # script

cmorlogs=$SCRATCH/cmorized-results/$EXP/logs # see sub-cmor.sh - should conform

# -- CHECK
if (( chck ))
then
    (( verbose )) && echo " -- $tag --"

    # - script log

    if [[ -f $log ]]
    then
        (( verbose )) && { echo; echo " -- Check $log --" ; 
                           grep "ECMWF.*INFO.*ExitCode" $log ; 
                           grep "ECMWF.*INFO.*State" $log ; }
        excode=$(sed -nr "s|.*INFO.* ExitCode *: ||"p $log)
        status=$(sed -nr "s|.*INFO.*State *: ||"p $log)
        second=$(sed -nr "s|.*INFO.* ElapsedRaw *: ||"p $log)
        if [[ $excode = '0:0' && $status = COMPLETED ]]; then
            (( verbose )) && echo "Looks like it went ok."
        fi
        echo -n "Runtime (hh:mm:ss): "
        echo $second | awk '{printf "%d:%02d:%02d\n", $1/3600, ($1/60)%60, $1%60}'
    else
        echo "submit script log not found: $log"
    fi

    # - ece2cmor3 log

    # regexp list of lines we can be safely ignored in the logs
    to_ignore="\
^! All files were closed successfully. $\|\
^! ------$\|^$\|^! $\|\
Unsupported combination of frequency monC\|\
ifs2cmor: Skipped removal of nonempty work directory\|\
Variable dissicnat            in table Oyr        was not found in the NEMO output files: task skipped.\|\
Variable talknat              in table Oyr        was not found in the NEMO output files: task skipped.\|\
The grid opa_grid_1point consists of a single point which is not supported, dismissing variables masso in Omon,volo in Omon,zostoga in Omon,thetaoga in Omon,bigthetaoga in Omon,tosga in Omon,soga in Omon,sosga in Omon\|\
Dimension ncatice could not be found in file.*lim_grid_T_2D.nc, inserting using length-one dimension instead\|\
Oclim\|monC\|level type 109, level -1."

    if [[ -d $cmorlogs ]]; then
        cd $cmorlogs
        (( verbose )) && echo "Checking logs in $cmorlogs"
    else
        (( verbose )) && echo "Checking logs in $PWD"
    fi
    
    set +e
    f=$(find . -name "${tag}-*cmor.log" -exec ls -1 {} \; | sort | tail -1)
    if [[ -f $f ]]
    then
        (( verbose )) && ( echo; echo " -- Check $f --")
        grep -v "$to_ignore" $f
    else
        echo "ECE2CMOR log for $tag not found"
    fi

    f=$(find . -name "${tag}-*[0-9].log" -exec ls -1 {} \; | sort | tail -1)
    if [[ -f $f ]]
    then
        (( verbose )) && ( echo; echo " -- Check $f --")
        grep -i "ERROR\|WARNING" $f | grep -v "$to_ignore"
        grep "Assumed previous month file for.*ICM.*+000000" $f
    else
        echo 'CMOR log not found'
    fi

    exit
fi

# -- read log
if (( page ))
then
    echo "Checking $log"
    ${PAGER:-less} $log
    exit
fi

# -- submit script
sbatch ${account:+--account=}${account:-} -J cmor_$tag \
       -o $log ${rundep:+-d afterok:}${rundep-} \
       sub-cmor.sh $EXP $COMPONENT $ILEG ${ocd-} ${ECEMODEL-}
