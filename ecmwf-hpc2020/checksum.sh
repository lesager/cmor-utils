#!/usr/bin/env bash

set -eu

(( $# != 1 )) && echo NEED EXP ARGUMENT && exit 1

EXP=$1

# Switch
NON="NON-"
NON=

datadir=$SCRATCH/cmorized-results/$EXP
#datadir=/ec/res4/scratch/nks/cmorized-results/$EXP #Twan

file_list="file.list."$NON$EXP
checksum_file="sha256sums-"$NON$EXP
mkdir -p log

sbatch <<EOF
#!/bin/bash
#SBATCH --job-name=csum-$NON$EXP
#SBATCH --qos=np
#SBATCH --ntasks=100
#SBATCH --nodes=1
#SBATCH --output="log/checksum-"$NON$EXP".log"

# Uses GNU parallel for parallelization of checksum computation. Make sure it's
# available! (on NSC, load module parallel/...)


cd $datadir

find -L ${NON}CMIP6 -type f > $file_list

/usr/bin/time parallel -a $file_list sha256sum > $checksum_file

#tar -cvf NONCMIP6-AerChem-$EXP.tar NON-CMIP6 $checksum_file $file_list

EOF
