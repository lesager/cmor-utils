#! /usr/bin/env bash

#SBATCH --qos=np
#SBATCH --time=06:00:00

set -eu

usage() {
    cat << EOT >&2

 ######################################################################################
 # CMORISE EC-Earth3 raw output from ONE experiment, ONE leg, and ONE model component #
 ######################################################################################

 SUBMIT
   sbatch [--job-name=<JOBNAME> ...] ${0##*/} <EXP> <COMPONENT> <LEG> 


 EXAMPLES

  In all examples, if there is a directory in the --output it must
  already exist for the log to be written!

    sbatch --output=abcd-nemo-001.out --job-name=abcd-nemo-001 ${0##*/} abcd nemo 1

  To submit several jobs at once (one exp, one model, several legs):

    exp=abcd
    model=ifs
    for i in {1..10}; do ${0##*/} \${exp} \${model} \$i; done

    and a more elaborate call:
    for i in {1..4}; do mess=\${exp}-\${model}-\$i; sbatch --output=log/\${mess}.out --job-name=\$mess ${0##*/} \${exp} \${model} \$i; done

  or to process all models output (one exp, one leg, all models):

    exp=abcd
    for model in ifs nemo tm5; do mess=\${exp}-\${model}-001; sbatch --o log/\${mess}.out -J \$mess ${0##*/} \${exp} \${model} 1; done

EOT
}

error() { echo "ERROR: $1" >&2; exit 1 ; }
urror() { echo "ERROR: $1" >&2;  usage; exit 1 ; }


# -- ARG
[[ "$#" -lt 3 ]]             && urror "Need AT LEAST THREE ARGUMENTS!"
[[ ! $1 =~ ^[a-Z_0-9]{4}$ ]] && urror "argument EXPERIMENT name (=$1) should be a 4-character string"
[[ ! $3 =~ ^[0-9]+$ ]]       && urror "argument LEG_NUMBER (=$3) should be a number"

EXP=$1
COMPONENT=$2
ILEG=$((10#$3))


# ECEDIR    directory with the raw ec-earth output results, for instance: t001/output/nemo/001
# ECEMODEL  name of the ec-earth model configuration, for instance: EC-EARTH-AOGCM
# METADATA  name of the meta data file, for instance: ece2cmor3/resources/metadata-templates/cmip6-CMIP-piControl-metadata-template.json
# VARSLIST  name of the variable list, in this case the so called json cmip6 data request file, for instance: cmip6-data-request-varlist-CMIP-piControl-EC-EARTH-AOGCM.json
# TEMPDIR   directory where ece2cmor3 is writting files during its execution
# ODIR      directory where ece2cmor3 will write the cmorised results of this job
# COMPONENT name of the model component for the current job to cmorise
# LEG       leg number for the current job to cmorise. Note for instance leg number one is written as 001.

LEG=$(printf "%03d" $ILEG)

# Experiment Definition (FIXME)
SUBDIR=FIXME
XPRMNT=historical ; ECEMODEL=EC-EARTH-AOGCM; MIP=CMIP

# EXAMPLES
# XPRMNT=historical ;     ECEMODEL=EC-EARTH-AOGCM;   MIP=CMIP ;;
# XPRMNT=1pctCO2 ;        ECEMODEL=EC-EARTH-AerChem; MIP=CMIP ;;
# XPRMNT=amip ;           ECEMODEL=EC-EARTH-AerChem; MIP=CMIP ;;
# XPRMNT=ssp370pdSST ;    ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
# XPRMNT=hist-piAer ;     ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
# XPRMNT=histSST-piCH4 ;  ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
# XPRMNT=histSST-piAer ;  ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
# XPRMNT=highres-future ; ECEMODEL=EC-EARTH3P-HR;    MIP=HighResMIP ;;
# XPRMNT=highres-future ; ECEMODEL=EC-EARTH3P-HR;    MIP=HighResMIP ;;
# XPRMNT=historical ;     ECEMODEL=EC-EARTH-HR;      MIP=CMIP ;;
# XPRMNT=piClim-aer ;     ECEMODEL=EC-EARTH-AerChem; MIP=RFMIP ;;

pextra='-pextra'
CTRLDIR=$SUBDIR/ctrl/output-control-files/cmip6${pextra}/${MIP}/${ECEMODEL}/cmip6-experiment-${MIP}-${XPRMNT}
CTRLDIR=$SUBDIR/ctrl/output-control-files/cmip6${pextra}/${MIP}/cmip6-experiment-${MIP}-${XPRMNT}

METADATA=$CTRLDIR/metadata*$COMPONENT-template.json # FIXME
VARSLIST=$CTRLDIR/*-varlist-*-$ECEMODEL.json        # FIXME

# Summary
echo "**INFO** METADATA="$METADATA
echo "**INFO** VARSLIST="$VARSLIST
echo "**INFO** ECEMODEL="$ECEMODEL


# Model output - FIXME
ECEDIR=$SCRATCH/ecearth3/$EXP/output/$COMPONENT/$LEG
[[ ! -d $ECEDIR ]] && error "Directory $ECEDIR does not exist"
[[ -z $(ls -A $ECEDIR) ]] && error "Empty dir: $ECEDIR"

# CMOR output
TEMPDIR=$SCRATCH/temp-cmor-dir/$EXP/$COMPONENT/$LEG
ODIR=$SCRATCH/cmorized-results/$EXP

if [ -d $TEMPDIR ]; then rm -rf $TEMPDIR; fi
mkdir -p $TEMPDIR
mkdir -p $ODIR/logs
##mkdir -p $LOGS

set +ue                         # if there is any error they will be logged

. $HPCPERM/mamba/etc/profile.d/conda.sh
conda activate ece2cmor3

#export HDF5_USE_FILE_LOCKING=FALSE
#export UVCDAT_ANONYMOUS_LOG=false

[[ ${XPRMNT:-dummy} = 'amip' ]] && refd="--refd 1960-01-01" || refd=

ece2cmor $ECEDIR --exp       $EXP      \
         --ececonf           $ECEMODEL \
         --$COMPONENT                  \
         --meta              $METADATA \
         --varlist           $VARSLIST \
         --tmpdir            $TEMPDIR  \
         --odir              $ODIR     \
         --npp               18        \
         --overwritemode     replace   \
         --log $refd
#    --skip_alevel_vars            \

mv -f $EXP-$COMPONENT-$LEG-*.log $ODIR/logs
if [ -d $TEMPDIR ]; then rm -rf $TEMPDIR; fi

