#!/bin/bash

# Check cmorization of one or more experiments
# 
# Has to be run in this directory. For example:
# 
#      cd [....]/cmor-utils/ecmwf-hpc2020
#      ./monitor.sh > $HOME/monitor-$(date +%F).log


date +%F

(( $# )) && exp="$@" || { echo 'need one or more 4-letter experiment names as args'; exit 1; }

for e in $exp
do
    echo
    echo "-----------------------------"
    echo "-------   $e   ------------"
    echo "-----------------------------"
    echo

    f=/scratch/nm6/ecearth3/$e/ece.info
    [[ ! -f $f ]] && echo "  $f does not exist. Skip $e" && continue

    nleg=$(tail -5 $f | sed -nr "s|leg_number=(.*)|\1|"p)

    [[ ! $nleg =~ ^[0-9]+$ ]] && echo "*EE* No leg found for $e !!!" && continue

    nstart=1
    [[ $e = amip ]] && nstart=20  # skip the spinup years
    [[ $e = xh2t ]] && nstart=102 # extension started at 102

    mods="tm5 ifs nemo"
    [[ $e = pcfa || $e = pcfc ]] && mods="tm5 ifs"

    for m in $mods
    do
        for k in $(eval echo {$nstart..$nleg})
        do
            echo "  $m $k"
            ./wrp-cmor.sh -c $e $m $k
        done
    done
done

cat << EOT

In case of problem, increase verbosity:

   ./wrp-cmor.sh -v -c onep ifs 11

EOT
