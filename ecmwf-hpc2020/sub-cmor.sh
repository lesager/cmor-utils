#! /usr/bin/env bash

#SBATCH --qos=np
#SBATCH --time=06:00:00

env | grep SLURM

set -eu
shopt -s nullglob

usage() {
    cat << EOT >&2

 ######################################################################################
 # CMORISE EC-Earth3 raw output from ONE experiment, ONE leg, and ONE model component #
 ######################################################################################

 SUBMIT
   sbatch [--job-name=<JOBNAME> ...] ${0##*/} <EXP> <COMPONENT> <LEG> [CTRLDIR] [ECEMODEL]

   EXP (4-letter experiment name) should be in the database (see
       script) or you need to provide at least the CTRLDIR.

 OPTIONAL ARG

   CTRLDIR: output control directory. Overwrite default, which relies
            on a database and a SUBDIR where to find the top ctrl dir.

   ECEMODEL: Default is set in the database. If not provided   

 EXAMPLES

  In all examples, if there is a directory in the --output it must
  already exist for the log to be written!

    sbatch --output=abcd-nemo-001.out --job-name=abcd-nemo-001 sub-cmor.sh abcd nemo 1

  To submit several jobs at once (one exp, one model, several legs):

    exp=abcd
    model=ifs
    for i in {1..10}; do sub-cmor.sh \${exp} \${model} \$i; done

    and a more elaborate call:
    for i in {1..4}; do mess=\${exp}-\${model}-\$i; sbatch --output=log/\${mess}.out --job-name=\$mess sub-cmor.sh \${exp} \${model} \$i; done

  or to process all models output (one exp, one leg, all models):

    exp=abcd
    for model in ifs nemo tm5; do mess=\${exp}-\${model}-001; sbatch --o log/\${mess}.out -J \$mess sub-cmor.sh \${exp} \${model} 1; done

  If you want to limit the number of concurrent jobs, say to 2, you
  use dependency and submit 2 series of jobs. Here starting from leg
  11 up to 66:

   rrrr=\$(sbatch sub-cmor.sh abcd ifs 11)
   for ((x = 13 ; x <= 66 ; x += 2)); do rrrr=\$(sbatch -d afterok:\${rrrr##* } sub-cmor.sh abcd ifs \${x}); done
   rrrr=\$(sbatch sub-cmor.sh abcd ifs 12)
   for ((x = 14 ; x <= 66 ; x += 2)); do rrrr=\$(sbatch -d afterok:\${rrrr##* } sub-cmor.sh abcd ifs \${x}); done

EOT
}

error() { echo "ERROR: $1" >&2; exit 1 ; }
urror() { echo "ERROR: $1" >&2;  usage; exit 1 ; }

get_file() {
    # Return one filename if there is only ONE satisfying the mask
    # input. Else raise an error. Rely on nullglob being set.
    files=( $1 )
    if [ ${#files[@]} -ne 1 ]; then
        echo "None or too many files (${#files[@]}): $1" >&2
        return 1
    fi
    echo ${files[0]} ; }

# -- ARG
[[ "$#" -lt 3 ]] && urror "Need AT LEAST THREE ARGUMENTS!"
[[ ! $1 =~ ^[a-Z_0-9]{4}$ ]] && urror "argument EXPERIMENT name (=$1) should be a 4-character string"
[[ ! $3 =~ ^[0-9]+$ ]] && urror "argument LEG_NUMBER (=$3) should be a number"

EXP=$1
COMPONENT=$2
ILEG=$((10#$3))

if [ "$#" -ge 4 ]; then
    ocd=$4
    [[ ! -d $ocd ]] && error "Directory $ocd does not exist"
    [[ -z $(ls -A $ocd) ]] && error "Empty dir: $ocd"
fi

[[ "$#" -ge 5 ]] && ECEMODEL=$5

# ECEDIR    directory with the raw ec-earth output results, for instance: t001/output/nemo/001
# ECEMODEL  name of the ec-earth model configuration, for instance: EC-EARTH-AOGCM
# METADATA  name of the meta data file, for instance: ece2cmor3/resources/metadata-templates/cmip6-CMIP-piControl-metadata-template.json
# VARSLIST  name of the variable list, in this case the so called json cmip6 data request file, for instance: cmip6-data-request-varlist-CMIP-piControl-EC-EARTH-AOGCM.json
# TEMPDIR   directory where ece2cmor3 is writting files during its execution
# ODIR      directory where ece2cmor3 will write the cmorised results of this job
# COMPONENT name of the model component for the current job to cmorise
# LEG       leg number for the current job to cmorise. Note for instance leg number one is written as 001.

LEG=$(printf "%03d" $ILEG)

# Experiment Definition - This is essentially a DATABASE of CMIP6 production runs
case $EXP in
    ao00) XPRMNT=historical ;     ECEMODEL=EC-EARTH-AOGCM;   MIP=CMIP ;;
    onep) XPRMNT=1pctCO2 ;        ECEMODEL=EC-EARTH-AerChem; MIP=CMIP ;;
    amip) XPRMNT=amip ;           ECEMODEL=EC-EARTH-AerChem; MIP=CMIP ;;
    s004) XPRMNT=ssp370pdSST ;    ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
    hpae) XPRMNT=hist-piAer ;     ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
    hsp4) XPRMNT=histSST-piCH4 ;  ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
    hspr) XPRMNT=histSST-piAer ;  ECEMODEL=EC-EARTH-AerChem; MIP=AerChemMIP ;;
    xh2t) XPRMNT=highres-future ; ECEMODEL=EC-EARTH3P-HR;    MIP=HighResMIP ;;
    s2hh) XPRMNT=highres-future ; ECEMODEL=EC-EARTH3P-HR;    MIP=HighResMIP ;;
    hi1y) XPRMNT=historical ;     ECEMODEL=EC-EARTH-HR;      MIP=CMIP ;;
    pic4) XPRMNT=piClim-aer ;     ECEMODEL=EC-EARTH-AerChem; MIP=RFMIP ;;
    *)
        [[ ! -d ${ocd:-dummy} ]] && urror "EXP not in the database, requires CTRLDIR (4th argument)"

        # Try to extract the ECEMODEL from CTRLDIR if not provided
        if [[ -z ${ECEMODEL:-} ]] ; then
            ECEMODEL=$(echo $ocd | sed -nr 's|.*MIP/(EC-EARTH[^/]*)/.*|\1|p')
            [[ -z $ECEMODEL ]] && urror "Could not extract ECEMODEL from CTRLDIR. Provide a 5th argument"
            echo "Using ECEMODEL=$ECEMODEL"
        fi
esac

# Experiment control output setup
if [[ -d ${ocd:-dummy} ]]
then
    CTRLDIR=$ocd
    # Expect only one metadata file per model and one data request per experiment
    METADATA=$(get_file "$CTRLDIR/metadata*$COMPONENT-template.json")
    VARSLIST=$(get_file "$CTRLDIR/*-varlist-*-$ECEMODEL.json")
else
    # DEFAULT: use the cmip6 requests we distribute with EC-Earth3,
    # which are defined by these two hardcoded parameters:
    SUBDIR=$PERM/ecearth3/ec-earth3/runtime/classic
    pextra=

    if [[ $MIP = 'CMIP' ]]
    then
        CTRLDIR=$SUBDIR/ctrl/output-control-files/cmip6${pextra}/${MIP}/${ECEMODEL}/cmip6-experiment-${MIP}-${XPRMNT}
    else
        CTRLDIR=$SUBDIR/ctrl/output-control-files/cmip6${pextra}/${MIP}/cmip6-experiment-${MIP}-${XPRMNT}
    fi
    METADATA=${CTRLDIR}/metadata-cmip6-${MIP}-${XPRMNT}-${ECEMODEL}-$COMPONENT-template.json
    VARSLIST=${CTRLDIR}/cmip6-data-request-varlist-${MIP}-${XPRMNT}-${ECEMODEL}.json
fi

# Summary
echo "**INFO** METADATA="$METADATA
echo "**INFO** VARSLIST="$VARSLIST
echo "**INFO** ECEMODEL="$ECEMODEL


# Model output
ECEDIR=$SCRATCH/ecearth3/$EXP/output/$COMPONENT/$LEG
[[ ! -d $ECEDIR ]] && error "Directory $ECEDIR does not exist"
[[ -z $(ls -A $ECEDIR) ]] && error "Empty dir: $ECEDIR"

# CMOR output
TEMPDIR=$SCRATCH/temp-cmor-dir/$EXP/$COMPONENT/$LEG
#ODIR=$SCRATCH/cmorized-results/${ECEMODEL}-${MIP}-${XPRMNT}/$EXP
ODIR=$SCRATCH/cmorized-results/$EXP
##LOGS=$HPCPERM/ece3runs/cmorization-logs/$EXP

if [ -d $TEMPDIR ]; then rm -rf $TEMPDIR; fi
mkdir -p $TEMPDIR
mkdir -p $ODIR/logs
##mkdir -p $LOGS

set +ue                         # if there is any error they will be logged

. $HPCPERM/mamba/etc/profile.d/conda.sh
conda activate ece2cmor3

#export HDF5_USE_FILE_LOCKING=FALSE
#export UVCDAT_ANONYMOUS_LOG=false

[[ ${XPRMNT:-dummy} = 'amip' ]] && refd="--refd 1960-01-01" || refd=

ece2cmor $ECEDIR --exp       $EXP      \
         --ececonf           $ECEMODEL \
         --$COMPONENT                  \
         --meta              $METADATA \
         --varlist           $VARSLIST \
         --tmpdir            $TEMPDIR  \
         --odir              $ODIR     \
         --npp               18        \
         --overwritemode     replace   \
         --log $refd
#    --skip_alevel_vars            \

mv -f $EXP-$COMPONENT-$LEG-*.log $ODIR/logs
if [ -d $TEMPDIR ]; then rm -rf $TEMPDIR; fi

